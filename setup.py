from setuptools import setup

setup(
    name="scheduler",
    version="0.0.1",
    packages=["scheduler", "simplified"],
    entry_points={
        "console_scripts": [
            "scheduler = scheduler.__main__:main",
            "simplified = simplified.__main__:main"
        ]
    },
)
