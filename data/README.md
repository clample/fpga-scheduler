## Single workload example
[scheduling-example.json](./scheduling-example.json) is an example of a single workload with 5 tasks. 

[scheduling-example-a.json](./scheduling-example-a.json), [scheduling-example-b.json](./scheduling-example-b.json), and [scheduling-example-c.json](./scheduling-example-c.json) are copies of scheduling-example.json with only the reconfiguration_speed field modified. These are used in section ??? to generate figures 4.1, 4.2, and 4.3 respectively.

Optimal schedules for these workloads, created by solving the Deiana MILP formulation to optimality, are stored in files [scheduling-example-result-a](./scheduling-example-result-a.json), [scheduling-example-result-b](./scheduling-example-result-b.json), and [scheduling-example-result-c](./scheduling-example-result-c.json).

[scheduling-example-makespan-v-reconfig-time.json](./scheduling-example-makespan-v-reconfig-time.json) is used for generating a schedule at many various reconfiguration speeds.
The result of this is included in [scheduling-example-makespan-v-reconfig-time-result.json](./scheduling-example-makespan-v-reconfig-time-result.json).
This is depicted in figure 4.4.

## Performance evaluation 

[performance-benchmark-2.json](./performance-benchmark-2.json) contains the workload used for benchmarking the three schedulers.

[performance-benchmark-2-result-simplified-model.json](./performance-benchmark-2-result-simplified-model.json) contains the result for solving with the simplified MILP model.
[performance-benchmark-2-result-iterative-k-2.json](./performance-benchmark-2-result-iterative-k-2.json) contains the result for solving with the iterative scheduler of Deiana et al.
[performance-benchmark-2-result-full-milp.json](./performance-benchmark-2-result-full-milp.json) contains the result of solving the MILP formulation of Deiana et al to optimality.

Regarding performance-benchmark-2-result-full-milp.json, only one sample containing 6 tasks was solved. The second sample remained unsolved after 9 hours (was at a 10% optimality gap when terminated).

These results are depicted in table 4.1 and table 4.2.

## Implementation Area vs Performance Trade-off

[many-workloads-many-reconfig-speeds.json](./many-workloads-many-reconfig-speeds.json) contains the original, randomly generated workload.
This is used in generating figure 4.5.
It is the "original workload" series in figures 4.9, 4.10.

[many-workloads-many-reconfig-speeds-improved.json](./many-workloads-many-reconfig-speeds-improved.json) contains the original workload, but transformed to improve the Area vs Perf trade-off of the FPGA implementations.
The "improved workload" series in figure 4.9.


[many-workloads-many-reconfig-speeds-shifted.json](./many-workloads-many-reconfig-speeds-shifted.json) contains the original workload, but transformed to worsen the Area vs Perf trade-off (aside from the most performant implementation).
The "sifted workload" series in figure 4.10.

[many-workloads-many-reconfig-speeds-iterative-solved.json](./many-workloads-many-reconfig-speeds-iterative-solved.json), [many-workloads-many-reconfig-speeds-simplified-solved.json](./many-workloads-many-reconfig-speeds-simplified-solved.json), [many-workloads-many-reconfig-speeds-improved-iterative-solved.json](./many-workloads-many-reconfig-speeds-improved-iterative-solved.json), [many-workloads-many-reconfig-speeds-improved-simplified-solved.json](./many-workloads-many-reconfig-speeds-improved-simplified-solved.json), [many-workloads-many-reconfig-speeds-shifted-iterative-solved.json](./many-workloads-many-reconfig-speeds-shifted-iterative-solved.json), and [many-workloads-many-reconfig-speeds-shifted-simplified-solved.json](./many-workloads-many-reconfig-speeds-shifted-simplified-solved.json) contain the results of scheduling these with the iterative scheduler and simplified MILP model.

[many-workloads-many-reconfig-speeds-2.json](./many-workloads-many-reconfig-speeds-2.json) contains the same workloads, with th eonly difference being that the range of reconfiguration speeds to be used is different.
[many-workloads-many-reconfig-speeds-2-iterative-solved.json](./many-workloads-many-reconfig-speeds-2-iterative-solved.json) and [many-workloads-many-reconfig-speeds-2-simplified-solved.json](./many-workloads-many-reconfig-speeds-2-iterative-solved.json) contain the results of solving.
Used in figures 4.6, 4.7, 4.8.

## Splitting tasks

[many-workloads-many-reconfig-speeds-unsplit-tasks.json](./many-workloads-many-reconfig-speeds-unsplit-tasks.json) contains the original, randomly generated workload.

[many-workloads-many-reconfig-speeds-split-tasks.json](./many-workloads-many-reconfig-speeds-split-tasks.json) contains the original workload, but with tasks split ("vertically") and a more performant FPGA implementation added to the split tasks.

These are used in generating figure 4.11.

[many-workloads-many-reconfig-speeds-unsplit-tasks-iterative-solved.json](./many-workloads-many-reconfig-speeds-unsplit-tasks-iterative-solved.json), [many-workloads-many-reconfig-speeds-unsplit-tasks-simplified-solved.json](./many-workloads-many-reconfig-speeds-unsplit-tasks-simplified-solved.json), [many-workloads-many-reconfig-speeds-split-tasks-iterative-solved.json](./many-workloads-many-reconfig-speeds-split-tasks-iterative-solved.json), and [many-workloads-many-reconfig-speeds-split-tasks-simplified-solved.json](./many-workloads-many-reconfig-speeds-split-tasks-simplified-solved.json) contain the results of scheduling these with the iterative scheduler and simplified MILP model.

[many-workloads-many-reconfig-speeds-split-tasks-2.json](./many-workloads-many-reconfig-speeds-split-tasks-2.json) and [many-workloads-many-reconfig-speeds-unsplit-tasks-2.json](./many-workloads-many-reconfig-speeds-unsplit-tasks-2.json) contain the same workloads, but with the rangee of reconfiguration speeds to be used changed.
[many-workloads-many-reconfig-speeds-split-tasks-2-iterative-solved.json](many-workloads-many-reconfig-speeds-split-tasks-2-iterative-solved.json), [many-workloads-many-reconfig-speeds-split-tasks-2-simplified-solved.json](many-workloads-many-reconfig-speeds-split-tasks-2-simplified-solved.json), [many-workloads-many-reconfig-speeds-unsplit-tasks-2-iterative-solved.json](many-workloads-many-reconfig-speeds-unsplit-tasks-2-iterative-solved.json), and [many-workloads-many-reconfig-speeds-unsplit-tasks-2-simplified-solved.json](many-workloads-many-reconfig-speeds-unsplit-tasks-2-simplified-solved.json) contain the results of scheduling.

