{ pkgs ? import <nixpkgs> {}}:

with pkgs;
let

  gurobipy = pkgs.python38Packages.buildPythonPackage rec {
    pname = "gurobipy";
    version = "9.1.2";
    format = "wheel";
    src = pkgs.python38Packages.fetchPypi {
      inherit pname version format;
      sha256 = "436851683f83a68f850abc30fb34c750aa12986b8ccb6439b1b8b27e2c32494a";
      python = "cp38";
      abi = "cp38";
      platform = "manylinux1_x86_64";
    };

    checkInputs = [];
    nativeBuildInputs = [];
    buildInputs = [];
    propagatedBuildInputs = [ ];
    
  };
  
  nsopy = pkgs.python38Packages.buildPythonPackage rec {
    pname = "nsopy";
    version = "1.52";
    src = pkgs.python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "8f41bdebb40aeed4ae702ed1bda352d7152dc88cb7cc005cc0e52e4eb4951dc4";
    };

    checkInputs = [];
    nativeBuildInputs = [];
    buildInputs = [  ];
    propagatedBuildInputs = with pkgs.python38Packages; [
      
      colorama
      numpy
      pandas
      py
      pytest
      python-dateutil
      pytz
      six
      gurobipy
    ];
  };
in
stdenv.mkDerivation rec {
  name = "deiana-scheduler";
  python = python38.withPackages(ps: with ps; [
    pyomo
    pytest
    hypothesis
    matplotlib
    marshmallow
    numpy
    nsopy
  ]);
  buildInputs = [ python glpk gurobi ];
}
