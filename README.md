# FPGA Scheduler

Run with `python -m scheduler`. Test with `python -m pytest`.

For debugging: `python -m pdb -m scheduler ...`

## Data

For reproducibility, all data used in the thesis is contained in this repository.
An explanation of which data was used in generating which figure is included [here](./data/README.md).
