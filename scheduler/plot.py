import matplotlib as mpl
mpl.use('pgf')
mpl.rcParams.update({
     # 'pgf.texsystem': 'luatex'
      'font.family': 'serif',
      'text.usetex': True,
      'pgf.rcfonts': False,
     #'figure.figsize': (3, 2)
})

import matplotlib.pyplot as plt
from itertools import cycle
from numpy import mean
from .data import *
import operator
import collections
from itertools import chain

# See https://matplotlib.org/stable/gallery/lines_bars_and_markers/horizontal_barchart_distribution.html
def plot_schedule(result):
    # plt.figure(figsize=[6,2.5])
    
    fig, ax = plt.subplots()

    components = { solved_task.selected_component for solved_task in result.schedule }    
    solved_tasks_by_component = { component.replace(' ', '\n', 1): [ solved_task for solved_task in result.schedule if solved_task.selected_component == component ]
                                  for component in components }
    
    
    for component in sorted(solved_tasks_by_component.keys(), reverse=True):
        for solved_task in solved_tasks_by_component[component]:
            ax.barh(component, solved_task.duration(), left=solved_task.task_start, color="gray", alpha=0.2, edgecolor="black")

            impl = result.workload.implementations[solved_task.selected_implementation]

            task_string = f"${solved_task.name}$"
            
            if isinstance(impl, HardwareImplementation):
                task_string = f"{task_string}\n{impl.clb_requirement}"
            
            ax.text(solved_task.task_start + (result.makespan / 100), component, task_string)

            

            # Note: Some SW tasks may be assigned unnecessary reconfigurations.
            # This does not increase the makespan and is just an artifact of the MILP model + Gurobi
            # (the model doesn't explicitly forbid it).
            # The simplest workaround is to just ignore them.
            if (solved_task.reconfig_start != None) and isinstance(impl, HardwareImplementation):
                ax.barh(component, solved_task.reconfig_duration(), left=solved_task.reconfig_start,  hatch='xxx', color="gray", alpha=0.2, edgecolor="black")
    
    plt.xlim(left=0)
    plt.show()
    # plt.savefig("schedule.pgf", format='pgf')

def plot_runtime(results):
    raise Exception("Need to update this now that the results object has changed")
    plt.figure(figsize=[5,4])

    runtime_vs_num_tasks = {}
    for result in results:
        # There will "reconfiguration" tasks in the task list as well.
        # Since we are only counting the number of concrete tasks, we check that the task has an implementation
        num_tasks = len(list(filter(lambda node: 'implementation' in node[1], result.task_graph.nodes(data=True))))

        if num_tasks not in runtime_vs_num_tasks:
            runtime_vs_num_tasks[num_tasks] = []
        runtime_vs_num_tasks[num_tasks].append(result.solve_time)

    
    x_vals = sorted(list(runtime_vs_num_tasks.keys()))
    y_vals = [ mean(runtime_vs_num_tasks[num_tasks])  for num_tasks in x_vals ]
    plt.plot(x_vals, y_vals)

    plt.xlabel("Number of tasks in workload")
    plt.ylabel("MILP time to solve (seconds)")
    plt.show()


def plot_makespan_vs_reconfig_time(results, output_file=None):
    #plt.figure(figsize=[4.5,3])

    x_vals = [ result.reconfig_time for result in results.results ]
    y_vals = [ result.makespan for result in results.results  ]

    plt.plot(x_vals, y_vals)
    plt.xlabel("Reconfiguration time per resource unit, $s_r$")
    plt.ylabel("Schedule makespan")
    # plt.show()
    # plt.xlim(left=0) Not sure if this makes things look better
    plt.savefig(output_file, format='pgf')

def plot_makespan_vs_reconfig_time_many_series(all_series_results, output_file=None):
    """
    Handle input data from many different series (ex: upper bound values and lower bound values)
    Also performs averaging of the makespands from multiple workloads.
    """

    plt.xlabel("Reconfiguration time per resource unit, $s_r$")
    plt.ylabel("Schedule makespan")
    for overall_result in all_series_results:
        x_vals = [
            many_workloads_single_reconfig_speed_result.reconfig_time
            for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
        ]
        y_vals = [
            mean(list(map(operator.attrgetter('makespan'), many_workloads_single_reconfig_speed_result.results)))
            for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
        ]
        line_style = "solid" if "Upper Bound" in overall_result.series_name else "dotted"
        transform_strings = ["Split", "Improved", "Shifted"]
        marker = "^" if any(transform_str in overall_result.series_name for transform_str in transform_strings) else None
        
        plt.plot(x_vals, y_vals, label=overall_result.series_name, linestyle=line_style, marker=marker)

    plt.legend(loc="upper left")
    # plt.show()
    plt.savefig(output_file, format='pgf')

def plot_avg_num_sw_tasks_vs_reconfig_time(overall_result, output_file=None):
    plt.xlabel("Reconfiguration time per resource unit, $s_r$")
    plt.ylabel("Average number of software implementations scheduled")

    x_vals = [
        many_workloads_single_reconfig_speed_result.reconfig_time
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]

    y_vals = [
        mean(list(map(lambda result: result.num_sw_impls_used(), many_workloads_single_reconfig_speed_result.results)))
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]

    plt.plot(x_vals, y_vals)
    #plt.show()
    plt.savefig(output_file, format='pgf')
    
def plot_solve_time_vs_num_tasks_many_series(all_series_results, output_file=None):
    """
    Plot the average time needed for the scheduler to solve a workload vs the number of tasks in the workload.
    """

    # plt.xlabel("Number of tasks")
    # plt.ylabel("Scheduler solve time (s)")

    for series_name, series_result in all_series_results.items():
        grouped_by_task_num = collections.defaultdict(list)
        for result in series_result:
            grouped_by_task_num[len(result.workload.task_list)].append(result.solve_time)            

        num_tasks, solve_times = zip(*grouped_by_task_num.items())
        x_vals = list(num_tasks)
        y_vals = [ mean(solve_times_for_num_tasks) for solve_times_for_num_tasks in solve_times ]

        # Useful for creating a latex table instead of a plot
        print(f"{series_name} result:")
        print(f"{x_vals}")
        print(f"{y_vals}")
        
        # plt.plot(x_vals, y_vals, label=series_name)

    # plt.legend(loc="upper left")
    # plt.show()
    # plt.savefig(output_file, format='pgf')

def plot_avg_num_partitions_vs_reconfig_time(overall_result, output_file=None):
    plt.xlabel("Reconfiguration time per resource unit, $s_r$")
    plt.ylabel("Average number of FPGA partitions")

    x_vals = [
        many_workloads_single_reconfig_speed_result.reconfig_time
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]
    y_vals = [
        mean(list(map(lambda result: result.num_fpga_partitions(), many_workloads_single_reconfig_speed_result.results)))
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]

    plt.plot(x_vals, y_vals)
    # plt.show()
    plt.savefig(output_file, format='pgf')

def plot_avg_implementation_size_vs_reconfig_time(overall_result, output_file=None):
    plt.xlabel("Reconfiguration time per resource unit, $s_r$")
    plt.ylabel("Average FPGA implementation size")

    x_vals = [
        many_workloads_single_reconfig_speed_result.reconfig_time
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]

    y_vals = [
        # TODO: mean() will return an error if there are no hardware implementations
        mean(list(chain.from_iterable([ result.implementation_sizes() for result in many_workloads_single_reconfig_speed_result.results ])))
        for many_workloads_single_reconfig_speed_result in overall_result.results_by_reconfig_speed
    ]

    plt.plot(x_vals, y_vals)
    # plt.show()
    plt.savefig(output_file, format='pgf')
