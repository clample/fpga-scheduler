from .data import *
from pyomo.environ import *
import itertools
import re
import math
import copy
import time

class DeianaScheduler:

    reconfiguration_task = staticmethod(lambda i: f"reconfiguration task {i}")
    
    def __init__(self):
        self.abstract_model = DeianaScheduler.create_abstract_model()
        self.solver_factory = SolverFactory('gurobi')

    # def solve_relaxation(self, workload):
    #     abstract_model = model()
    #     instance = create_instance(workload, abstract_model)
    #     opt = SolverFactory('glpk')
    #     opt.options['nomip'] = None
    #     results = opt.solve(instance)
    #     # instance.pprint()
    #     if results.solver.termination_condition == TerminationCondition.infeasible:
    #         return None
    #     return instance.time_cost.value
    
    def solve_iteratively(self, workload):
        """
        Solve the schedule using the iterative scheduling algorith described in
        Multiobjective Reconfiguration-Aware Scheduler for FPGA-Based Heterogeneous Architectures.
        
        Tasks are added a few at a time and then solved using the MILP formulation.
        For each invocation of the MILP solver, some variables are fixed based on the previous solution.
        """
        start_time = time.process_time()
        k = 2 # TODO: Run with a higher value for actual figure
        task_worklist = copy.copy(workload.task_list)
        active_tasks = []
        instance = None
        while task_worklist:
            for i in range(min(k, len(task_worklist))):
                active_tasks.append(task_worklist.pop())
            tmp_workload = Workload(
                active_tasks,
                workload.reconfiguration_speed,
                workload.fpga_resources,
                workload.hardware_components,
                workload.software_components
            )
            data = self.model_data(tmp_workload)
            new_instance = self.abstract_model.create_instance(data)
            if instance:
                # Note:
                # There doesn't seem to be a nice way of accessing the domain/index of a variable set, so I've just hardcoded them.
                # All of the variables fixed below are Boolean. I assume that rounding them with `round` is sound.
                # I'm not aware of a better way to fix all of the below variables other than to do it variable-by-variable.
                for tic in instance.task_assigned_component_implementation_index:
                    new_instance.task_assigned_component_implementation[tic].fix(
                        round(instance.task_assigned_component_implementation[tic].value)
                    )
                for tt in instance.component_precedence:
                    new_instance.preceeding_task_follows_immediately_on_component[tt].fix(
                        round(instance.preceeding_task_follows_immediately_on_component[tt].value)
                    )
                for ct in instance.task_map_hardware:
                    new_instance.first_task_on_component[ct].fix(
                        round(instance.first_task_on_component[ct].value)
                    )                    
                for rt_t in instance.reconfiguration_required_index:
                    new_instance.reconfiguration_required[rt_t].fix(
                        round(instance.reconfiguration_required[rt_t].value)
                    )
            results = self.solver_factory.solve(new_instance)
            instance = new_instance

            if results.solver.termination_condition == TerminationCondition.infeasible:
                return None

        if not instance:
            raise Exception("Unable to solve iteratively. Maybe there were no tasks?")
        
        return self.process_result(workload, instance, time.process_time() - start_time)
                
    
    def solve(self, workload):
        data = self.model_data(workload)
        instance = self.abstract_model.create_instance(data)
        
        # https://stackoverflow.com/questions/51522718/get-gurobi-iis-using-pyomo-and-gurobipy
        # In case the model is infeasible
        # results = opt.solve(instance, tee=True, options_string="ResultFile=model.ilp")
        results = self.solver_factory.solve(instance)
        if results.solver.termination_condition == TerminationCondition.infeasible:
            return None
        return self.process_result(workload, instance, results.solver.time)

    def process_result(self, workload, solved_instance, solve_time):
        reconfig_map = {
            task: reconfig for (reconfig, task) in solved_instance.reconfiguration_required_index
            if round(solved_instance.reconfiguration_required[(reconfig, task)].value) == 1
        }

        solved_tasks = []
        for task in workload.tasks.values():
            task_begin_time = solved_instance.begin_time[task.name].value
            task_end_time = solved_instance.end_time[task.name].value
            task_assigned_component_implementation = [
                (c, i)
                for (t, i, c) in solved_instance.task_assigned_component_implementation_index
                if t == task.name and round(solved_instance.task_assigned_component_implementation[(t,i,c)].value) == 1
            ]

            # Each task should be assigned one impl and one component:
            assert len(task_assigned_component_implementation) == 1        
            component, implementation = task_assigned_component_implementation[0]

            reconfig_begin_time = None
            reconfig_end_time = None
            if task.name in reconfig_map:
                reconfig_task = reconfig_map[task.name]
                reconfig_begin_time = solved_instance.begin_time[reconfig_task].value
                reconfig_end_time = solved_instance.end_time[reconfig_task].value
            solved_task = SolvedTask(task.name, implementation, component, task_begin_time, task_end_time, reconfig_begin_time, reconfig_end_time)
            solved_tasks.append(solved_task)        
        return Result(solved_tasks, workload, solve_time, solved_instance.time_cost.value)

    def get_resource_requirements(self, hardware_implementations):
        resource_requirements = {}
        for impl in hardware_implementations:
            resource_requirements[(impl.name, 'CLB')] = impl.clb_requirement
            resource_requirements[(impl.name, 'DSP')] = impl.dsp_requirement
            resource_requirements[(impl.name, 'BRAM')] = impl.bram_requirement
        return resource_requirements

    def model_data(self, workload):
        # Note: The minimum_time_unit value seems a bit tricky.
        # Based on my reading of starts_after_other_starts_semantics_constraint,
        # it should be smaller than any possible difference in start times between two tasks.
        # As long as task runtimes have integer values, they can only differ by a time unit of 1.
        # Taking reconfiguration into account, they could also differ by the time needed to reconfigure 1 unit of bitstream.
        # To be safe, this is then divided by 2.
        # Special case where reconfiguration speed is 0.
        minimum_time_unit = 1 if workload.reconfiguration_speed == 0 else float(min(workload.reconfiguration_speed, 1)) / 2

        hardware_components = workload.hardware_components
        software_components = workload.software_components
        resources = workload.resources
        resource_density = workload.resource_density
        # Note that reconfiguration_speed is the time needed to reconfigure one unit of bitstream
        # TODO: It isn't really a speed - so the name should be changed. reconfiguration_time doesn't make sense though either.
        reconfiguration_speed = workload.reconfiguration_speed

        max_bitstream_size = max(map(lambda impl: impl.bitstream_size(resource_density), workload.hardware_implementations))

        max_execution_time = workload.get_max_execution_time()
        reconfiguration_tasks = list(map(DeianaScheduler.reconfiguration_task, range(len(workload.tasks))))

        task_map = workload.get_task_map()

        task_map_possible_mappings = [ (task, impl, component) for ((task, impl,component), value) in task_map.items() if value == 1 ]

        # Use `set` to remove duplicate elements from the list
        task_map_possible_implementations = list(set([(task, impl) for (task, impl, component) in task_map_possible_mappings]))
        task_map_possible_components = list(set([(task, component) for (task, impl, component) in task_map_possible_mappings]))

        task_map_hardware = [
            (task, component)
            for (task, component) in task_map_possible_components
            if component in hardware_components
        ]

        resource_requirements = self.get_resource_requirements(workload.hardware_implementations)

        component_precedence = [
            (task1, task2)
            for task1 in workload.tasks.keys()
            for task2 in workload.tasks.keys()
            if task1 != task2
            # and (task2, task2) not in precedence_closure
            and not set(
                component for (task, component) in task_map_possible_components if task == task1
            ).isdisjoint(component for (task, component) in task_map_possible_components if task == task2)
        ]

        overlapping_tasks = [
            (task1, task2)
            for task1 in itertools.chain(reconfiguration_tasks, workload.tasks.keys())
            for task2 in itertools.chain(reconfiguration_tasks, workload.tasks.keys())
            if task1 != task2
            and not (task1 in reconfiguration_tasks and task2 in reconfiguration_tasks)
            # and (task1, task2) not in precedence_closure
            # and (task2, task1) not in precedence_closure
        ]

        compatible_tasks = [
            (task1, task2)
            for task1 in workload.tasks.keys()
            for task2 in workload.tasks.keys()
            if task1 != task2
            and not set(
                (impl, component) for (task, impl, component) in task_map_possible_mappings if task == task1
            ).isdisjoint((impl, component) for (task, impl, component) in task_map_possible_mappings if task == task2)
        ]

        overlapping_tasks_shared_component = [
            (task1, task2, component)
            for (task1, task2) in overlapping_tasks
            for component in itertools.chain(hardware_components, software_components)
            if (task1, component) in task_map_possible_components
            and (task2, component) in task_map_possible_components
        ]

        preceeding_tasks_with_hardware_implementation = [
            (task1, task2, impl.name)
            for (task1, task2) in component_precedence
            for impl in workload.hardware_implementations
            if (task1, impl.name) in task_map_possible_implementations
            and (task1, task2) in compatible_tasks
        ]

        execution_times = { impl.name: impl.execution_time for impl in workload.implementations.values() }

        return {None: {
            'max_execution_time': { None: max_execution_time },
            'minimum_time_unit': { None: minimum_time_unit },
            'hardware_components': { None: hardware_components },
            'software_components': { None: software_components },
            'resources': { None: resources },
            'resource_density': resource_density,
            'reconfiguration_speed': { None: reconfiguration_speed },
            'max_bitstream_size': { None: max_bitstream_size},
            'fpga_resources': workload.fpga_resources,
            'tasks': { None: list(workload.tasks.keys()) },
            'reconfiguration_tasks': { None: reconfiguration_tasks },
            'software_implementations': { None: [ impl.name for impl in workload.software_implementations ] },
            'hardware_implementations': { None: [ impl.name for impl in workload.hardware_implementations ] },
            'resource_requirements': resource_requirements,
            'execution_time': execution_times,
            'task_map_possible_mappings': { None: task_map_possible_mappings },
            'task_map_possible_implementations': { None: task_map_possible_implementations },
            'task_map_possible_components': { None: task_map_possible_components },
            'task_map_hardware': { None: task_map_hardware },
            'precedences': { None: [] },
            'precedence_closure': { None: [] },
            'component_precedence': { None: component_precedence },
            'overlapping_tasks': { None: overlapping_tasks },
            'compatible_tasks': { None: compatible_tasks },
            'task_map': task_map,
            'overlapping_tasks_shared_component': { None: overlapping_tasks_shared_component },
            'preceeding_tasks_with_hardware_implementation': { None: preceeding_tasks_with_hardware_implementation}
        }}

    def create_abstract_model():
        model = AbstractModel()

        # See T_max
        model.max_execution_time = Param(within=NonNegativeReals)
        model.minimum_time_unit = Param(within=NonNegativeReals)

        # Target architecture description
        model.hardware_components = Set()
        model.software_components = Set()
        model.components = model.hardware_components | model.software_components
        # See R
        model.resources = Set()
        # See bit_r
        model.resource_density = Param(model.resources, within=NonNegativeIntegers)
        # See T_rec
        model.reconfiguration_speed = Param(within=NonNegativeReals)
        # See bit_max
        model.max_bitstream_size = Param(within=NonNegativeIntegers)

        # Taskgraph description
        model.tasks = Set()
        model.reconfiguration_tasks = Set()
        model.all_tasks = model.tasks | model.reconfiguration_tasks
        model.software_implementations = Set()
        model.hardware_implementations = Set()
        model.all_implementations = model.software_implementations | model.hardware_implementations
        # See P
        model.precedences = Set(within=model.tasks * model.tasks)
        model.precedence_closure = Set(within=model.tasks * model.tasks)
        # TODO: Is time an integer or a real?...
        model.execution_time = Param(model.all_implementations, within=NonNegativeIntegers)

        # See map_t,i,c
        model.task_map = Param(model.tasks * model.all_implementations * model.components, within=Boolean)

        # See TIC, TI, TC in the extended MILP formulation from Deiana et. al
        model.task_map_possible_mappings = Set(within=model.tasks * model.all_implementations * model.components)
        model.task_map_possible_implementations = Set(within=model.tasks * model.all_implementations)
        model.task_map_possible_components = Set(within=model.tasks * model.components)
        # This is not explicitly part of the Deiana schedule, but needed to work with Pyomo
        model.task_map_hardware = Set(within=model.tasks * model.hardware_components)


        # TODO: Are resource requirements real or floats?
        # See res
        model.resource_requirements = Param(model.hardware_implementations * model.resources, within=NonNegativeIntegers)
        # See maxRes
        model.fpga_resources = Param(model.resources, within=NonNegativeIntegers)

        # See CP, OT, CT in extended MILP formulation from Deiana et. al
        model.component_precedence = Set(within=model.tasks * model.tasks)
        model.overlapping_tasks = Set(within=model.all_tasks * model.all_tasks)
        # Note - for CT to be complete (t1, t2) in CT => (t2, t1) in CT
        model.compatible_tasks = Set(within=model.tasks * model.tasks)

        # This is not explicitly part of the Deiana schedule, but needed to work with Pyomo
        model.overlapping_tasks_shared_component = Set(within=model.overlapping_tasks * model.components)
        model.preceeding_tasks_with_hardware_implementation = Set(within=model.component_precedence * model.hardware_implementations)


        model.begin_time = Var(model.all_tasks, bounds=(0, model.max_execution_time), within=NonNegativeReals)
        model.end_time = Var(model.all_tasks, bounds=(0, model.max_execution_time), within=NonNegativeReals)

        # See mic, mi, mc in extended MILP formulation from Deiana et. al
        # These variables indicate if a task is assigned a particular component, or implementation
        model.task_assigned_component_implementation = Var(model.tasks * model.all_implementations * model.components, within=Boolean)
        model.task_assigned_implementation = Var(model.tasks * model.all_implementations, within=Boolean)
        model.task_assigned_component = Var(model.tasks * model.components, within=Boolean)

        # See oc - amount of resources of type r needed by hardware component c
        model.component_resource_requirements = Var(model.hardware_components * model.resources, within=NonNegativeReals)

        # See cp - set to true/1 if task t2 is executed right after task t1 on the same hardware component
        # TODO: What do we mean by "right after"
        model.preceeding_task_follows_immediately_on_component = Var(model.component_precedence, within=Boolean)

        # See cft - set to true/1 if task t is the first task executed on hardware component c
        # TODO: This should only be applied when component is a hardware component
        model.first_task_on_component = Var(model.task_map_possible_components, within=Boolean)

        # See rtt- set to true if the reconfiguration task rt is required before the execution of task t
        model.reconfiguration_required = Var(model.reconfiguration_tasks * model.tasks, within=Boolean)

        # See rtc - set to true if reconfiguration task rt is performed on hardware component c
        # Note that reconfiguration_required_on_component might be true even if a reconfiguration isn't required on the particular component
        # This is because the model doesn't have any constraints guaranteeing this.
        # It would be possible to add additional constraints, but that might also make the model more complicated.
        model.reconfiguration_required_on_component = Var(model.reconfiguration_tasks * model.hardware_components, within=Boolean)

        # See bitc - the bitstream size for hardware component c
        model.bitc = Var(model.hardware_components, within=NonNegativeReals)

        # See ba - Set to true if task t1 begins after the beginning of task t2 or at the same time of t2
        model.task_starts_after_other_starts = Var(model.overlapping_tasks, within=Boolean)
        # See bb - Set to true if t1 begins before the end of task t2 or at the end of t2
        model.task_starts_before_other_finishes = Var(model.overlapping_tasks, within=Boolean)
        # See bo - Real variable in the range [0,1] set to 1 if the beginning of task t1 overlaps in time with task t2
        model.tasks_overlap = Var(model.overlapping_tasks, within=UnitInterval)

        # See T_cost: The total time of the schedule
        model.time_cost = Var(within=NonNegativeReals)

        model.task_assigned_component_implementation_constraint = Constraint(
            model.tasks, model.all_implementations, model.components,
            rule=DeianaScheduler.task_assigned_component_implementation_constraint
        )

        # See equation 2
        model.starts_before_other_finishes_semantics = Constraint(
            model.overlapping_tasks,
            rule=DeianaScheduler.starts_before_other_finishes_semantics_constraint
        )
        model.starts_after_other_starts_semantics = Constraint(
            model.overlapping_tasks,
            rule=DeianaScheduler.starts_after_other_starts_semantics_constraint
        )
        model.task_overlap_semantics = Constraint(
            model.overlapping_tasks,
            rule=DeianaScheduler.tasks_overlap_semantics_constraint
        )

        # See equation 3
        model.one_component_and_implementation_per_task = Constraint(
            model.tasks,
            rule=DeianaScheduler.one_component_and_implementation_per_task_constraint
        )

        # See equation 4
        model.task_assigned_implementation_semantics = Constraint(
            model.task_map_possible_implementations,
            rule=DeianaScheduler.task_assigned_implementation_semantics_constraint
        )
        model.task_assigned_component_semantics = Constraint(
            model.task_map_possible_components,
            rule=DeianaScheduler.task_assigned_component_semantics_constraint
        )

        # See equation 5
        model.end_time_constraint = Constraint(
            model.tasks,
            rule=DeianaScheduler.end_time_constraint
        )

        # See equation 6
        model.one_first_task_per_hardware_component_constraint = Constraint(
            model.hardware_components,
            rule=DeianaScheduler.one_first_task_per_hardware_component_constraint
        )

        # See equation 7
        model.if_first_task_on_component_then_task_mapped_to_component_constraint = Constraint(
            model.task_map_hardware,
            rule=DeianaScheduler.if_first_task_on_component_then_task_mapped_to_component_constraint
        )

        # See equation 8
        model.task_follows_immediately_semantics_constraint = Constraint(
            model.component_precedence,
            model.components,
            rule=DeianaScheduler.task_follows_immediately_semantics_constraint
        )

        # See equation 9
        model.first_task_or_follows_other_task_constraint = Constraint(
            model.task_map_hardware,
            rule=DeianaScheduler.first_task_or_follows_other_task_constraint
        )

        # See equation 10
        model.start_and_end_times_consistent_with_task_following_other_task_constraint = Constraint(
            model.component_precedence,
            rule=DeianaScheduler.start_and_end_times_consistent_with_task_following_other_task_constraint
        )

        # See equation 11
        model.hardware_component_has_sufficient_resources_constraint = Constraint(
            model.hardware_components,
            model.resources,
            model.tasks,
            rule=DeianaScheduler.hardware_component_has_sufficient_resources_constraint
        )

        # See equation 12
        model.bitstream_size_constraint = Constraint(
            model.hardware_components,
            rule=DeianaScheduler.bitstream_size_constraint
        )

        # See equation 13
        model.reconfiguration_applies_to_at_most_one_task_constraint = Constraint(
            model.reconfiguration_tasks,
            rule=DeianaScheduler.reconfiguration_applies_to_at_most_one_task_constraint
        )
        model.task_requires_at_most_one_reconfiguration = Constraint(
            model.tasks,
            rule=DeianaScheduler.task_requires_at_most_one_reconfiguration
        )

        # See equation 14
        # Note: The index of the constraint was a bit tricky for this one
        model.reconfiguration_required_on_component_semantics = Constraint(
            model.reconfiguration_tasks,
            model.task_map_hardware,
            rule=DeianaScheduler.reconfiguration_required_on_component_semantics
        )

        # See equation 15
        model.reconfiguration_tasks_end_after_beginning = Constraint(
            model.reconfiguration_tasks,
            rule=DeianaScheduler.reconfiguration_tasks_end_after_beginning
        )

        # See equation 16
        model.reconfiguration_is_sufficiently_long = Constraint(
            model.reconfiguration_tasks,
            model.hardware_components,
            rule=DeianaScheduler.reconfiguration_is_sufficiently_long
        )

        # See equation 17
        model.task_dependencies_constraint = Constraint(
            model.precedences,
            rule=DeianaScheduler.task_dependencies_constraint
        )

        # See equation 18
        model.no_task_overlap_on_component_constraint = Constraint(
            model.overlapping_tasks_shared_component,
            rule=DeianaScheduler.no_task_overlap_on_component_constraint
        )

        # See equation 19
        model.one_subsequent_task_constraint = Constraint(
            model.tasks,
            rule=DeianaScheduler.one_subsequent_task_constraint
        )
        model.one_previous_task_constraint = Constraint(
            model.tasks,
            rule=DeianaScheduler.one_previous_task_constraint
        )

        # See equation 20
        model.one_reconfiguration_at_a_time_constraint = Constraint(
            model.reconfiguration_tasks,
            rule=DeianaScheduler.one_reconfiguration_at_a_time_constraint
        )

        # See equation 21
        model.fpga_resources_constraint = Constraint(
            model.resources,
            rule=DeianaScheduler.fpga_resources_constraint
        )

        # See equation 22
        model.reconfiguration_required_to_change_implementation = Constraint(
            model.preceeding_tasks_with_hardware_implementation,
            rule=DeianaScheduler.reconfiguration_required_to_change_implementation
        )
        model.reconfiguration_required_between_incompatible_tasks = Constraint(
            model.tasks,
            rule=DeianaScheduler.reconfiguration_required_between_incompatible_tasks
        )

        # See equation 23
        model.reconfiguration_task_finishes_before_task = Constraint(
            model.reconfiguration_tasks,
            model.tasks,
            rule=DeianaScheduler.reconfiguration_task_finishes_before_task
        )
        model.reconfiguration_task_starts_after_task = Constraint(
            model.reconfiguration_tasks,
            model.component_precedence,
            rule=DeianaScheduler.reconfiguration_task_starts_after_task
        )

        # See equation 25
        model.time_cost_constraint = Constraint(
            model.all_tasks,
            rule=DeianaScheduler.time_cost_constraint
        )

        model.reconfiguration_for_first_task = Constraint(
            model.tasks,
            rule=DeianaScheduler.reconfiguration_for_first_task
        )

        model.task_assigned_component_and_overlap_semantics = Constraint(
            model.overlapping_tasks_shared_component,
            rule=DeianaScheduler.task_assigned_component_and_overlap_semantics
        )

        model.task_assigned_component_and_overlap_semantics2 = Constraint(
            model.overlapping_tasks_shared_component,
            rule=DeianaScheduler.task_assigned_component_and_overlap_semantics2
        )

        model.objective = Objective(rule=DeianaScheduler.objective_expression, sense=minimize)

        return model

    def objective_expression(model):
        return model.time_cost

    # TODO: Do I somehow ruin the performance by doing the thing described below? Sounds like it increases the number of variables or constraints or something? Might be worth describing in the thesis?
    # Custom constraint
    # In Deiana's paper, mic, mi, mc are only defined over mic, mi, and mc
    # This becomes an issue in equation 8 (task_follows_immediately_semantics_constraint)
    # This equation is over all (t1,t2) in CP and all components - but task1 might not be compatible with the component
    # As a solution, lets instead have mic, mi, mc defined over all task * component * implementation
    # This constraint will insure that mic is 0 whenever the mapping isn't actually permitted
    def task_assigned_component_implementation_constraint(model, task, implementation, component):
        return (None, model.task_assigned_component_implementation[(task, implementation, component)], model.task_map[(task, implementation, component)])

    # See equation 2 in extended MILP formulation from Deiana et al
    def starts_before_other_finishes_semantics_constraint(model, task1, task2):
        task1_begin = model.begin_time[task1]
        task2_end = model.end_time[task2]
        starts_before_other_finishes = model.task_starts_before_other_finishes[(task1, task2)] * model.max_execution_time
        # If task1 begins before task2 ends, then starts_before_other_finishes will be true
        # TODO: How do we ensure the converse?
        return (0, task1_begin - task2_end + starts_before_other_finishes, None)

    # See equation 2 in extended MILP formulation from Deiana et al
    def starts_after_other_starts_semantics_constraint(model, task1, task2):
        task1_begin = model.begin_time[task1]
        task2_begin = model.begin_time[task2]
        starts_after_other_starts = model.task_starts_after_other_starts[(task1, task2)] * model.max_execution_time
        # If task1 begins after task2 begins (or at the same time), then starts_after_other_starts will be true
        return (0, task2_begin - task1_begin - model.minimum_time_unit + starts_after_other_starts, None)

    # See equation 2 in extended MILP formulation from Deiana et al
    def tasks_overlap_semantics_constraint(model, task1, task2):
        overlapping_tasks = (task1, task2)
        t1_starts_after_t2_starts = model.task_starts_after_other_starts[overlapping_tasks]
        t1_starts_before_t2_finishes = model.task_starts_before_other_finishes[overlapping_tasks]
        tasks_overlap = model.tasks_overlap[overlapping_tasks]
        return (None, t1_starts_after_t2_starts + t1_starts_before_t2_finishes - tasks_overlap, 1)

    # See equation 3 in extended MILP formulation from Deiana et al
    def one_component_and_implementation_per_task_constraint(model, task):
        mappings = [(t,i,c) for (t,i,c) in model.task_map_possible_mappings if t == task]
        return (1, sum(model.task_assigned_component_implementation[mapping] for mapping in mappings), 1)

    # See equation 4
    def task_assigned_implementation_semantics_constraint(model, task, implementation):
        mappings = [(t,i,c) for (t,i,c) in model.task_map_possible_mappings if t == task and i == implementation]
        return (0, model.task_assigned_implementation[(task, implementation)] - sum(model.task_assigned_component_implementation[mapping] for mapping in mappings), 0)

    # See equation 4
    def task_assigned_component_semantics_constraint(model, task, component):
        mappings = [(t,i,c) for (t,i,c) in model.task_map_possible_mappings if t == task and c == component]
        return (0, model.task_assigned_component[(task, component)] - sum(model.task_assigned_component_implementation[mapping] for mapping in mappings), 0)

    # See equation 5
    def end_time_constraint(model, task):
        end_time = model.end_time[task]
        begin_time = model.begin_time[task]
        mappings = [(t,i) for (t,i) in model.task_map_possible_implementations if t == task]
        execution_time = sum(
            model.task_assigned_implementation[(t,i)] * model.execution_time[i]
            for (t,i) in mappings
        )
        return (0, end_time - (begin_time + execution_time), 0)

    # See equation 6
    def one_first_task_per_hardware_component_constraint(model, hardware_component):
        mappings = [(t,c) for (t,c) in model.task_map_possible_components if c == hardware_component]
        return (None, sum(model.first_task_on_component[mapping] for mapping in mappings), 1)

    # See equation 7
    def if_first_task_on_component_then_task_mapped_to_component_constraint(model, task, component):
        return (0, model.task_assigned_component[(task, component)] - model.first_task_on_component[(task, component)], None)

    # See equation 8
    def task_follows_immediately_semantics_constraint(model, task1, task2, component1):
        task1_on_component1 = model.task_assigned_component[(task1, component1)]
        task2_on_other_component = sum(
            model.task_assigned_component[(t,c)]
            for (t,c) in model.task_map_possible_components if t == task2 and c != component1
        )
        task2_preceeds_task1_on_component = model.preceeding_task_follows_immediately_on_component[(task1, task2)]
        return (None, task1_on_component1 + task2_on_other_component + task2_preceeds_task1_on_component, 2)

    # See equation 9
    # If task is assigned to hardware_component,
    # then it is either the first task on that component or it follows another task
    def first_task_or_follows_other_task_constraint(model, task, hardware_component):
        first_task_on_component = model.first_task_on_component[(task, hardware_component)]
        follows_other_task = sum(
            model.preceeding_task_follows_immediately_on_component[(other_task, task)]
            for (other_task, t) in model.component_precedence if t == task
        )
        task_assigned_component = model.task_assigned_component[(task, hardware_component)]
        return (0, first_task_on_component + follows_other_task - task_assigned_component, None)

    # See equation 10
    # If task2 follows task1 (cp/preceeding_task_follows_immediately_on_component)
    # then task2 must begin after task1
    def start_and_end_times_consistent_with_task_following_other_task_constraint(model, task1, task2):
        task2_begin_time = model.begin_time[task2]
        task1_end_time = model.end_time[task1]
        task2_not_following_task1 = (1 - model.preceeding_task_follows_immediately_on_component[(task1, task2)]) * model.max_execution_time
        return (0, task2_begin_time - task1_end_time + task2_not_following_task1, None)

    # See equation 11
    # Hardware component has sufficient resources
    def hardware_component_has_sufficient_resources_constraint(model, hardware_component, resource, task):
        component_resource_requirement = model.component_resource_requirements[(hardware_component, resource)]
        task_resource_requirement = sum(
            model.task_assigned_component_implementation[(t,i,c)] * model.resource_requirements[(i,resource)]
            for (t,i,c) in model.task_map_possible_mappings if t == task and c == hardware_component
        )
        return (0, component_resource_requirement - task_resource_requirement, None)

    # See equation 12
    def bitstream_size_constraint(model, hardware_component):
        bitstream_size = model.bitc[hardware_component]
        required_bitstream_size = sum(
            model.component_resource_requirements[(hardware_component, resource)] * model.resource_density[resource]
            for resource in model.resources
        )
        return (0, bitstream_size - required_bitstream_size, 0)

    # See equation 13
    def reconfiguration_applies_to_at_most_one_task_constraint(model, reconfiguration_task):
        tasks_reconfigured_by_reconfiguration_task = sum(
            model.reconfiguration_required[(reconfiguration_task, task)]
            for task in model.tasks
        )
        return (None, tasks_reconfigured_by_reconfiguration_task, 1)

    # See equation 13
    # TODO: I would expect this to already be handled by the objective function
    def task_requires_at_most_one_reconfiguration(model, task):
        reconfiguration_tasks_required_by_task = sum(
            model.reconfiguration_required[(reconfiguration_task, task)]
            for reconfiguration_task in model.reconfiguration_tasks
        )
        return (None, reconfiguration_tasks_required_by_task, 1)

    # See equation 14
    def reconfiguration_required_on_component_semantics(model, reconfig_task, task, hardware_component):
        reconfig_required_on_component = model.reconfiguration_required_on_component[(reconfig_task, hardware_component)]
        reconfig_required_for_task = model.reconfiguration_required[(reconfig_task, task)]
        task_assigned_component = model.task_assigned_component[(task, hardware_component)]
        return (None, reconfig_required_for_task + task_assigned_component - reconfig_required_on_component, 1)

    # See equation 15
    def reconfiguration_tasks_end_after_beginning(model, reconfig_task):
        return (0, model.end_time[reconfig_task] - model.begin_time[reconfig_task], None)

    # See equation 16
    # When a reconfiguration task is performed on a component,
    # It's duration should be long enough to reconfigure the full bitstream size
    def reconfiguration_is_sufficiently_long(model, reconfig_task, hardware_component):
        end_time = model.end_time[reconfig_task]
        begin_time = model.begin_time[reconfig_task]
        bitstream_size = model.bitc[hardware_component]
        reconfig_required = model.reconfiguration_required_on_component[(reconfig_task, hardware_component)]
        # Note that this is negative if no reconfiguration is required
        required_reconfig_time = (bitstream_size - (1 - reconfig_required) * model.max_bitstream_size) * model.reconfiguration_speed
        return (None, begin_time + required_reconfig_time - end_time, 0)

    # See equation 17
    def task_dependencies_constraint(model, task1, task2):
        return (0, model.begin_time[task2] - model.end_time[task1], None)

    # See equation 18
    def no_task_overlap_on_component_constraint(model, task1, task2, component):
        tasks_overlap = model.tasks_overlap[(task1, task2)]
        task1_assigned_component = model.task_assigned_component[(task1, component)]
        task2_assigned_component = model.task_assigned_component[(task2, component)]
        return (None, tasks_overlap + task1_assigned_component + task2_assigned_component, 2)

    # See equation 19
    def one_subsequent_task_constraint(model, task):
        task_pairs = [(task, task2) for (task1, task2) in model.component_precedence if task1 == task]
        return (None, sum(model.preceeding_task_follows_immediately_on_component[task_pair] for task_pair in task_pairs), 1)

    # See equation 19
    def one_previous_task_constraint(model, task):
        task_pairs = [(task1, task) for (task1, task2) in model.component_precedence if task2 == task]
        return (None, sum(model.preceeding_task_follows_immediately_on_component[task_pair] for task_pair in task_pairs), 1)

    # See equation 20
    def one_reconfiguration_at_a_time_constraint(model, rt):
        # TODO: This is a bit of a hack, there must be a nicer way
        reconfiguration_task_pattern = re.compile(r"reconfiguration task (\d+)")
        reconfiguration_task_index = int(reconfiguration_task_pattern.match(rt).group(1))
        if reconfiguration_task_index == 0:
            return Constraint.Skip
        return (0, model.begin_time[DeianaScheduler.reconfiguration_task(reconfiguration_task_index)] - model.end_time[DeianaScheduler.reconfiguration_task(reconfiguration_task_index - 1)], None)

    # See equation 21
    def fpga_resources_constraint(model, resource):
        return (0, sum(model.component_resource_requirements[(component, resource)] for component in model.hardware_components), model.fpga_resources[resource])

    # See equation 22
    # When task2 immediately follows task1 on a component _and_ task2 uses a different implementation than implementatin1
    # Then a reconfiguration is required before task2
    def reconfiguration_required_to_change_implementation(model, task1, task2, implementation1):
        t2_follows_t1 = model.preceeding_task_follows_immediately_on_component[(task1, task2)]
        t1_assigned_impl1 = model.task_assigned_implementation[(task1, implementation1)]
        task2_uses_other_implementation = sum(
            model.task_assigned_implementation[(task2, impl2)]
            for (t2, impl2) in model.task_map_possible_implementations
            if impl2 != implementation1 and impl2 in model.hardware_implementations
        )
        reconfiguration_needed = sum(
            model.reconfiguration_required[(reconfiguration_task, task2)]
            for reconfiguration_task in model.reconfiguration_tasks
        )
        return (0, 2 + reconfiguration_needed - t2_follows_t1 - t1_assigned_impl1 - task2_uses_other_implementation, None)

    # See equation 22
    def reconfiguration_required_between_incompatible_tasks(model, task):
        reconfiguration_needed = sum(
            model.reconfiguration_required[(reconfiguration_task, task)]
            for reconfiguration_task in model.reconfiguration_tasks
        )
        task_follows_incompatible_task = sum(
            model.preceeding_task_follows_immediately_on_component[(task2, task)]
            for (task2, t) in model.component_precedence
            if t == task and not (t, task2) in model.compatible_tasks
        )
        return (0, reconfiguration_needed - task_follows_incompatible_task, None)

    # See equation 23
    def reconfiguration_task_finishes_before_task(model, reconfiguration_task, task):
        task_begin_time = model.begin_time[task]
        reconfiguration_end_time = model.end_time[reconfiguration_task]
        reconfiguration_required = model.reconfiguration_required[(reconfiguration_task, task)]

        return (0, task_begin_time - reconfiguration_end_time + (1 - reconfiguration_required) * model.max_execution_time, None)

    # See equation 23
    def reconfiguration_task_starts_after_task(model, reconfiguration_task, task1, task2):
        task1_end_time = model.end_time[task1]
        reconfiguration_begin_time = model.begin_time[reconfiguration_task]
        reconfiguration_required = model.reconfiguration_required[(reconfiguration_task, task2)]
        task2_follows_task1 = model.preceeding_task_follows_immediately_on_component[(task1, task2)]
        return (0, reconfiguration_begin_time - task1_end_time + (2 - reconfiguration_required - task2_follows_task1) * model.max_execution_time, None)


    # See equation 25
    def time_cost_constraint(model, task):
        return (0, model.time_cost - model.end_time[task], None)

    # See equation 30
    def task_assigned_component_and_overlap_semantics(model, task1, task2, component):
        val = (model.task_starts_before_other_finishes[(task1, task2)] +
               model.task_starts_before_other_finishes[(task2, task1)] -
               model.task_assigned_component[(task1, component)] -
               model.task_assigned_component[(task2, component)] +
               1)
        return (0, val, None)

    # See equation 30 again
    def task_assigned_component_and_overlap_semantics2(model, task1, task2, component):
        val = (model.task_starts_after_other_starts[(task1, task2)] +
               model.task_starts_after_other_starts[(task2, task1)] -
               model.task_assigned_component[(task1, component)] -
               model.task_assigned_component[(task2, component)] +
               1)
        return (0, val, None)

    # TODO: Would be good to add cutting plane which canonicalizes the possible schedules
    # I'm a bit confused how it should work in the case of one component, for example, though.
    # The first task would have t = c and therefore couldn't be assigned to the only component.

    # Adding constraint to enforce that the first task on a HW component also has a reconfiguration
    def reconfiguration_for_first_task(model, task):
        has_reconfiguration = sum(
            model.reconfiguration_required[(reconfiguration_task, task)]
            for reconfiguration_task in model.reconfiguration_tasks
        )
        is_first_task_on_hw = sum(
            model.first_task_on_component[(task, hw_component)]
            for hw_component in model.hardware_components
        )

        return (0, has_reconfiguration - is_first_task_on_hw, None)
