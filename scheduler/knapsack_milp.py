from pyomo.environ import *

class KnapsackSolver:
    def __init__(self):
        self.abstract_model = KnapsackSolver.create_abstract_model()
        self.solver_factory = SolverFactory('gurobi')

    def create_abstract_model():
        model = AbstractModel()
        model.possible_items = Set()
        model.tasks = Set()
        model.task_groups = Set(model.tasks)
        model.capacity = Param(within=Integers)
        model.value = Param(model.possible_items, within=Reals)
        model.weight = Param(model.possible_items, within=NonNegativeIntegers)
        model.is_selected = Var(model.possible_items, within=Boolean)
        
        model.objective = Objective(rule=KnapsackSolver.optimize_value, sense=maximize)
        model.within_weight_limit_constraint = Constraint(rule=KnapsackSolver.within_weight_limit_constraint)
        model.tasks_scheduled_at_most_once_constraint = Constraint(model.tasks, rule=KnapsackSolver.tasks_scheduled_at_most_once_constraint)
        return model

    def optimize_value(model):
        return sum(model.value[item] * model.is_selected[item] for item in model.possible_items )

    def within_weight_limit_constraint(model):
        return (None, sum(model.weight[item] * model.is_selected[item] for item in model.possible_items), model.capacity)

    def tasks_scheduled_at_most_once_constraint(model, task):
        return (None, sum(model.is_selected[item] for item in model.task_groups[task]) , 1)

    def solve(self, items, capacity):
        data = self.model_data(items, capacity)
        instance = self.abstract_model.create_instance(data)
        # TODO: Validate that a valid solution was obtained (not infeasible, etc)
        self.solver_factory.solve(instance)
        return self.process_result(items, instance)

    def model_data(self, items, capacity):
        item_names = []
        item_values = {}
        item_weights = {}
        task_groups = {}
        for index, item in enumerate(items):
            item_name = f"item_{index}"
            item["name"] = item_name
            item_names.append(item_name)
            item_values[item_name] = item["value"]
            item_weights[item_name] = item["weight"]
            if "task" in item:
                task = item["task"]
                if task not in task_groups:
                    task_groups[task] = []
                task_groups[task].append(item_name)
            

        return { None: {
            'possible_items': { None: item_names },
            'capacity': { None: capacity },
            'tasks': { None: task_groups.keys() },
            'value': item_values,
            'weight': item_weights,
            'task_groups': task_groups
        }}

    def process_result(self, items, solved_instance):
        for item in items:
            item_name = item["name"]
            # float => boolean
            item["is_selected"] = solved_instance.is_selected[item_name].value > 0.9
        return {
            "items": items,
            "value": value(solved_instance.objective)
        }
