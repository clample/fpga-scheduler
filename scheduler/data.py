from typing import List, Dict
from dataclasses import dataclass, field
from marshmallow import Schema, fields, post_load
import itertools
import math
import numpy as np
from . import num_regions

@dataclass
class HardwareImplementation:
    name: str
    clb_requirement: int
    dsp_requirement: int
    bram_requirement: int
    execution_time: int

    def configuration_time(self, resource_density, reconfiguration_speed):
        return self.bitstream_size(resource_density) * reconfiguration_speed

    def bitstream_size(self, resource_density):
        return resource_density['CLB'] * self.clb_requirement + resource_density['DSP'] * self.dsp_requirement + resource_density['BRAM'] * self.bram_requirement

    # Equivalent to oc_{i,r}
    # TODO: Probably just makes more sense to structure the resource requirements as a map?
    def resource_requirement(self, resource):
        if resource == "CLB":
            return self.clb_requirement
        elif resource == "DSP":
            return self.dsp_requirement
        elif resource == "BRAM":
            return self.bram_requirement
        else:
            raise Exception(f"Unexpected resource {resource}")

    # TODO: Make this a field rather than a method - then we don't have to keep allocating these (would improve performance)
    def resource_requirements(self):
        return np.array([
            self.clb_requirement,
            self.dsp_requirement,
            self.bram_requirement
        ])
    
class HardwareSchema(Schema):
    name = fields.String()
    clb_requirement = fields.Integer()
    dsp_requirement = fields.Integer()
    bram_requirement = fields.Integer()
    execution_time = fields.Integer()

    @post_load
    def make_impl(self, data, **kwargs):
        return HardwareImplementation(**data)

@dataclass
class SoftwareImplementation:
    name: str
    execution_time: int
    
class SoftwareSchema(Schema):
    name = fields.String()
    execution_time = fields.Integer()
    compatible_components = fields.List(fields.String())

    @post_load
    def make_impl(self, data, **kwargs):
        return SoftwareImplementation(**data)


class Task:
    def __init__(self, name, hardware_implementations, software_implementations):
        self.name = name
        self.hardware_implementations = hardware_implementations
        self.software_implementations = software_implementations
        self.implementations = hardware_implementations + software_implementations

class TaskSchema(Schema):
    name = fields.String()
    hardware_implementations = fields.List(fields.Nested(HardwareSchema))
    software_implementations = fields.List(fields.Nested(SoftwareSchema))

    @post_load
    def make_task(self, data, **kwargs):
        return Task(**data)

class Workload:

    hardware_component_format = staticmethod(lambda i: f"FPGA Partition {i}")
    software_component_format = staticmethod(lambda i: f"SW Processor {i}")

    @staticmethod
    def create_workload(task_list, reconfiguration_speed, num_software_components, fpga_resources):
        region_solver = num_regions.RegionSolver()
        num_hardware_components_required = region_solver.solve(task_list, fpga_resources)
        hardware_components = list(map(Workload.hardware_component_format, range(num_hardware_components_required)))
        software_components = list(map(Workload.software_component_format, range(num_software_components)))
        return Workload(task_list, reconfiguration_speed, fpga_resources, hardware_components, software_components)

    def __init__(self, task_list, reconfiguration_speed, fpga_resources, hardware_components, software_components):
        self.task_list = task_list
        self.tasks = { task.name: task for task in task_list }
        hardware_implementations = { hw_impl.name: hw_impl for task in self.tasks.values() for hw_impl in task.hardware_implementations }
        software_implementations = { sw_impl.name: sw_impl for task in self.tasks.values() for sw_impl in task.software_implementations }
        self.implementations = { **hardware_implementations, **software_implementations }
        # It's more convenient to have hw_impls and sw_impls be lists rather than dicts
        self.hardware_implementations = hardware_implementations.values()
        self.software_implementations = software_implementations.values()

        self.reconfiguration_speed = reconfiguration_speed

        self.fpga_resources = fpga_resources
        self.fpga_resources_np = np.array([
            fpga_resources['CLB'],
            fpga_resources['DSP'],
            fpga_resources['BRAM']
        ])
        self.resources = ['CLB', 'DSP', 'BRAM']
        self.resource_density = { 'CLB': 1, 'DSP': 1, 'BRAM': 1 }

        self.hardware_components = hardware_components 
        self.software_components = software_components 
        self.components = self.hardware_components + self.software_components
            
    def get_task_map(self):
        task_map = {
            (task, impl, component): 0
            for task in self.tasks.keys()
            for impl in self.implementations.keys()
            for component in self.components
        }
                    
        for task in self.tasks.values():
            for impl in task.hardware_implementations:
                for component in self.hardware_components:
                    task_map[(task.name, impl.name, component)] = 1
            for impl in task.software_implementations:
                for component in self.software_components:
                    task_map[(task.name, impl.name, component)] = 1
        return task_map

    # Returns an upper bound on the total schedule makespan
    # Upper bound assumes worst case scenario of:
    # 1. All tasks + reconfigurations are run serially
    # 2. Every task runs with its slowest implementation
    # 3. Every task (including the first) requires a reconfiguration. The reconfiguration time is based on the worst implementation.
    # Point 3 is necessary since if a small and large implementation are scheduled on the same component,
    # both tasks will require the same longer reconfiguration.
    # The 
    def get_max_execution_time(self):
        """
        Returns an upper bound on the total schedule makespan
        Upper bound assumes worst case scenario of:
            1. All tasks + reconfigurations are run serially
            2. Every task runs with its slowest implementation
            3. Every task (including the first) requires a reconfiguration. The reconfiguration time is based on the worst implementation.
        Point 3 is necessary since if a small and large implementation are scheduled on the same component,
        both tasks will require the same longer reconfiguration.

        This is also a valid upper bound for the simplified scheduling problem, but a tighter bound can be obtained with simplified_scheduling_upper_bound
        """
        longest_reconfiguration = max(map(
            lambda impl: impl.configuration_time(self.resource_density, self.reconfiguration_speed),
            self.hardware_implementations
        ), default=0)
        total_max_execution_time = 1 + (longest_reconfiguration * len(self.tasks))
        for task in self.tasks.values():
            total_max_execution_time += max(map(lambda impl: impl.execution_time, task.implementations))
        return math.ceil(total_max_execution_time)

    def simplified_scheduling_upper_bound(self):
        """
        Like get_max_execution_time, but specialized for the simplified scheduling problem to be tighter.

        Unlike get_max_execution_time - the reconfiguration time doesn't need to handle the case where small and large implementations are scheduled on the same component.
        This is because in the simplfied problem, the reconfiguration time only depends on the size of the implementation.
        """
        # TODO: Maybe this makes sense. Having a tight upper bound for z_U isn't the highest priority now, though.
        #       Just use get_max_execution_time for now.
        pass

    def simplified_scheduling_lower_bound(self):
        """
        Computes simple lower bounds on the makespan. 
        The lower bound is a lower bound on both the simplified scheduling problem.
        It assumes that every FPGA impl (including the first) requires a reconfiguration, 
        so it isn't actually a lower bound on Deiana's formulation of the problem.         
        """
        minimum_processing_times = {}
        for task in task.values():
            implementations = task.implementations
            implementation_runtimes = map(lambda impl: self.get_impl_runtime(impl), implementations)
            # NOTE: This assumes that all implementations can potentially be run on given hardware.
            #       The resource requirements of all implementations should be supported by the FPGA.
            #       Their should actually be a CPU if there are any software components.
            #       If this isn't verified during workload creation, it should be verified here.
            minimum_processing_times[task] = min(implementation_runtimes)        
        return max(minimum_processing_times.values())
    
    def get_impl_runtime(self, impl):
        if isinstance(impl, SoftwareImplementation):
            return impl.execution_time
        elif isinstance(impl, HardwareImplementation):
            return impl.execution_time + impl.configuration_time(self.resource_density, self.reconfiguration_speed)
        else:
            raise Exception(f"Unrecognized implementation type. Expected HW or SW implementation.")

class WorkloadSchema(Schema):
    task_list = fields.List(fields.Nested(TaskSchema))
    reconfiguration_speed = fields.Float(allow_none=True)
    num_software_components = fields.Function(lambda obj: len(obj.software_components), deserialize=lambda obj: obj)
    fpga_resources = fields.Dict(keys=fields.String(), values=fields.Integer())

    @post_load
    def make_workload(self, data, **kwargs):
        return Workload.create_workload(**data)


#### Figure specific schemas ####
# Every figure for the thesis requires it's own particular format for the results.
# This unfortunately becomes a blit clunky.
#################################
    
@dataclass
class ManyReconfigSpeedsWorkload:
    workload: Workload
    min_reconfig_time: float
    max_reconfig_time: float
    num_samples: int

class ManyReconfigSpeedsWorkloadSchema(Schema):
    workload = fields.Nested(WorkloadSchema)
    min_reconfig_time = fields.Float()
    max_reconfig_time = fields.Float()
    num_samples = fields.Integer()

    @post_load
    def make_obj(self, data, **kwargs):
        return ManyReconfigSpeedsWorkload(**data)

@dataclass
class ManyWorkloadsManyReconfigSpeeds:
    workloads: List[Workload]
    min_reconfig_time: float
    max_reconfig_time: float
    num_samples: int

class ManyWorkloadsManyReconfigSpeedsSchema(Schema):
    workloads = fields.List(fields.Nested(WorkloadSchema))
    min_reconfig_time = fields.Float()
    max_reconfig_time = fields.Float()
    num_samples = fields.Integer()

    @post_load
    def make_obj(self, data, **kwargs):
        return ManyWorkloadsManyReconfigSpeeds(**data)
    
class SolvedTask:
    def __init__(self, name, selected_implementation, selected_component, task_start, task_end, reconfig_start, reconfig_end):
        self.name = name
        self.selected_implementation = selected_implementation
        self.selected_component = selected_component
        self.task_start = task_start
        self.task_end = task_end
        self.reconfig_start = reconfig_start
        self.reconfig_end = reconfig_end

    def duration(self):
        return self.task_end - self.task_start

    def reconfig_duration(self):
        return self.reconfig_end - self.reconfig_start

    def is_sw_impl_used(self):
        # Checking the string isn't a great solution, will work since "sw" is in the name if and only if it is a software implementation
        return "SW" in self.selected_component

class SolvedTaskSchema(Schema):
    name = fields.String()
    selected_implementation = fields.String()
    selected_component = fields.String()
    task_start = fields.Float()
    task_end = fields.Float()
    # TODO: These fields may be None (if the task is run on the SW processor). Is that handled OK?
    reconfig_start = fields.Float(allow_none=True)
    reconfig_end = fields.Float(allow_none=True)

    @post_load
    def make_solved_task(self, data, **kwargs):
        return SolvedTask(**data)
        
@dataclass
class Result:
    schedule: List[SolvedTask]
    workload: Workload
    solve_time: float
    makespan: float

    def num_sw_impls_used(self):
        return sum(1 if solved_task.is_sw_impl_used() else 0 for solved_task in self.schedule)

    def num_fpga_partitions(self):
        return len({ solved_task.selected_component for solved_task in self.schedule })

    def implementation_sizes(self):
        implementation_sizes = []
        for solved_task in self.schedule:
            if not solved_task.is_sw_impl_used():
                selected_implementation = self.workload.implementations[solved_task.selected_implementation]
                implementation_size = selected_implementation.clb_requirement
                implementation_sizes.append(implementation_size)
        return implementation_sizes
    
class ResultSchema(Schema):
    schedule = fields.List(fields.Nested(SolvedTaskSchema))
    workload = fields.Nested(WorkloadSchema)
    solve_time = fields.Float()
    makespan = fields.Float()
    
    @post_load
    def make_result(self, data, **kwargs):
        return Result(**data)

@dataclass
class ManyReconfigSpeedsSingleResult:
    schedule: List[SolvedTaskSchema]
    makespan: float
    reconfig_time: float

class ManyReconfigSpeedsSingleResultSchema(Schema):
    schedule = fields.List(fields.Nested(SolvedTaskSchema))
    makespan = fields.Float()
    reconfig_time = fields.Float()

    @post_load
    def make_obj(self, data, **kwargs):
        return ManyReconfigSpeedsSingleResult(**data)
    
@dataclass
class ManyReconfigSpeedsOverallResult:
    workload: ManyReconfigSpeedsWorkload
    results: List[ManyReconfigSpeedsSingleResult]

class ManyReconfigSpeedsOverallResultSchema(Schema):
    workload = fields.Nested(ManyReconfigSpeedsWorkloadSchema)
    results = fields.List(fields.Nested(ManyReconfigSpeedsSingleResultSchema))

    @post_load
    def make_obj(self, data, **kwargs):
        return ManyReconfigSpeedsOverallResult(**data)

@dataclass
class ManyWorkloadsSingleReconfigSpeedResult:
    reconfig_time: float
    results: List[Result]

class ManyWorkloadsSingleReconfigSpeedResultSchema(Schema):
    reconfig_time = fields.Float()
    results = fields.List(fields.Nested(ResultSchema))

    @post_load
    def make_result(self, data, **kwargs):
        return ManyWorkloadsSingleReconfigSpeedResult(**data)
    
@dataclass
class ManyWorkloadsManyReconfigSpeedsResult:
    # Workloads will end up being stored once per ManyWorkloadsSingleReconfigSpeedResult
    # This is redundant and will make the resulting object unnecessarily large.
    # It's not worth optmizing at the moment, though.
    series_name: str
    results_by_reconfig_speed: List[ManyWorkloadsSingleReconfigSpeedResult]
    
class ManyWorkloadsManyReconfigSpeedsResultSchema(Schema):
    series_name = fields.String()
    results_by_reconfig_speed = fields.List(fields.Nested(ManyWorkloadsSingleReconfigSpeedResultSchema))

    @post_load
    def make_result(self, data, **kwargs):
        return ManyWorkloadsManyReconfigSpeedsResult(**data)
