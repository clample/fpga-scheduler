from .data import *
from pyomo.environ import *

# TODO: For now I simply ignore other FPGA resources and only consider CLB. Maybe makes sense to generalize.

def solve(workload):

    data = model_data(workload)
    abstract_model = model()
    instance = abstract_model.create_instance(data)
    opt = SolverFactory('gurobi')
    results = opt.solve(instance)
    # instance.pprint()
    if results.solver.termination_condition == TerminationCondition.infeasible:
        return None
    return process_result(workload, instance, results.solver.time)

def process_result(workload, solved_instance, solve_time):
    solved_tasks = []

    assignments = {
        task: (component, impl) for (task, component, impl) in solved_instance.possible_assignments
        if round(solved_instance.task_assignment[(task, component, impl)].value) == 1
    }
    
    for task in workload.tasks.values():
        component, implementation = assignments[task.name]

        # The begin/end times don't have any meaning in the simplified scheduling model
        task_begin_time = None
        task_end_time = None
        reconfiguration_begin_time = None
        reconfiguration_end_time = None
        
        solved_task = SolvedTask(task.name, implementation, component, task_begin_time, task_end_time, reconfiguration_begin_time, reconfiguration_end_time)
    
    return Result(solved_tasks, workload, solve_time, solved_instance.makespan.value)

def model_data(workload):

    resource_requirements = get_resource_requirements(workload.hardware_implementations)
    execution_times = { impl.name: impl.execution_time for impl in workload.implementations.values() }

    possible_assignments = [
        (task, component, impl)
        for ((task, impl, component), value) in workload.get_task_map().items()
        if value == 1
    ]
    
    return { None: {
        'tasks': { None: workload.tasks.keys() },
        'reconfiguration_speed': { None: workload.reconfiguration_speed },
        'resources': { None: workload.resources },
        'fpga_resources': workload.fpga_resources,
        'hardware_components': { None: workload.hardware_components },
        'software_components': { None: workload.software_components },
        'software_implementations': { None: [ impl.name for impl in workload.software_implementations ] },
        'hardware_implementations': { None: [ impl.name for impl in workload.hardware_implementations ] },
        'resource_requirements': resource_requirements,
        'execution_time': execution_times,
        'possible_assignments':  { None: possible_assignments }
    }}

def model():
    model = AbstractModel()

    model.tasks = Set()
    model.reconfiguration_speed = Param(within=NonNegativeReals)
    model.resources = Set()
    model.fpga_resources = Param(model.resources, within=NonNegativeIntegers)

    model.hardware_components = Set()
    model.software_components = Set()
    model.components = model.hardware_components | model.software_components

    model.software_implementations = Set()
    model.hardware_implementations = Set()
    model.all_implementations = model.software_implementations | model.hardware_implementations

    model.resource_requirements = Param(model.hardware_implementations * model.resources, within=NonNegativeIntegers)
    model.execution_time = Param(model.all_implementations, within=NonNegativeIntegers)

    model.possible_assignments = Set(within= model.tasks * model.components * model.all_implementations)
    
    model.posible_assignments = Set(within=model.tasks * model.components * model.all_implementations)
    model.task_assignment = Var(model.possible_assignments, within=Boolean)
    model.resource_usage = Var(model.resources * model.hardware_components, within=NonNegativeIntegers)
    model.makespan = Var(within=NonNegativeReals)

    model.objective = Objective(rule=objective_expression, sense=minimize)

    model.task_run_once = Constraint(
        model.tasks,
        rule=task_run_once_constraint
    )

    model.hardware_component_makespan = Constraint(
        model.hardware_components,
        rule=hardware_component_makespan_constraint
    )

    model.software_component_makespan = Constraint(
        model.software_components,
        rule=software_component_makespan_constraint
    )

    model.resource_usage_per_component = Constraint(
        model.hardware_components, model.tasks, model.resources,
        rule=resource_usage_per_component_constraint
    )

    model.total_resource_usage_constraint = Constraint(
        model.resources,
        rule=total_resource_usage_constraint
    )
    
    return model

def objective_expression(model):
    return model.makespan

# See equation 5.2b
def task_run_once_constraint(model, task):
    mappings_for_task = [ (t, c, i) for (t, c, i) in model.possible_assignments if t == task]
    return (1, sum(model.task_assignment[mapping] for mapping in mappings_for_task) , 1)

# See equation 5.2c
def hardware_component_makespan_constraint(model, hardware_component):
    mappings_for_component = [ (t,c,i) for (t,c,i) in model.possible_assignments if c == hardware_component ]
    required_time = sum(
        (model.execution_time[i]
         + sum(model.reconfiguration_speed * model.resource_requirements[(i, r)] for r in model.resources)) * model.task_assignment[(t,c,i)]
        for (t,c,i) in mappings_for_component
    )
    return (None,
            required_time - model.makespan
            , 0)

# See equation 5.2d
def software_component_makespan_constraint(model, software_component):
    mappings_for_component = [ (t,c,i) for (t,c,i) in model.possible_assignments if c == software_component ]
    return (None,
            sum( model.execution_time[i] * model.task_assignment[(t,c,i)] for (t,c,i) in mappings_for_component) - model.makespan,
            0)

# See equation 5.2e
def resource_usage_per_component_constraint(model, hardware_component, task, resource):
    relevant_implementations = [ i for (t,c,i) in model.possible_assignments
                                 if c == hardware_component and t == task ]
    return (None,
            sum(model.resource_requirements[(implementation, resource)] * model.task_assignment[(task, hardware_component, implementation)]
                for implementation in relevant_implementations) - model.resource_usage[(resource, hardware_component)],
            0)

# See equation 5.2f
def total_resource_usage_constraint(model, resource):
    return (None, sum(model.resource_usage[(resource, hw_component)] for hw_component in model.hardware_components), model.fpga_resources[resource])

def get_resource_requirements(hardware_implementations):
    resource_requirements = {}
    for impl in hardware_implementations:
        resource_requirements[(impl.name, 'CLB')] = impl.clb_requirement
        resource_requirements[(impl.name, 'DSP')] = impl.dsp_requirement
        resource_requirements[(impl.name, 'BRAM')] = impl.bram_requirement
    return resource_requirements
