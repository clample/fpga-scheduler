import copy
from dataclasses import dataclass
from typing import Dict

# TODO: Maybe there are even faster ways to solve the knapsack problem? Maybe Gurobi is faster?
#       I think Gurobi (or some other alg) will be much faster especially when the makespan becomes very large
#       Maybe worth writing up a comparison and including it in the thesis?
#       Martello and Toth have written a book on the knapsack problem lol


# Solve the standard 0-1 knapsack problem
def solve(items, capacity):
    value = [[None for weight in range(capacity + 1)] for item in range(len(items) + 1)]
    knapsack_subproblem(items, capacity, value)
    determine_knapsack_items(items, capacity, value)
    return {
        "value": value[len(items)][capacity],
        "items": items
    }

def knapsack_subproblem(items, capacity, value):
    index = len(items)
    if index == 0 or capacity <= 0:
        value[index][capacity] = 0 
        return

    item, *remaining_items = items
    if not value[index - 1][capacity]:
        knapsack_subproblem(remaining_items, capacity, value)

    if item["weight"] > capacity:
        value[index][capacity] = value[index - 1][capacity]
    else:
        if not value[index - 1][capacity - item["weight"]]:
            knapsack_subproblem(remaining_items, capacity - item["weight"], value)
        with_item = value[index - 1][capacity - item["weight"]]
        without_item = value[index - 1][capacity]
        if with_item + item["value"] > without_item:
            value[index][capacity] = with_item + item["value"]
        else:
            value[index][capacity] = without_item

def determine_knapsack_items(items, capacity, value):
    index = len(items)
    while index >= 1 and capacity > 0:
        item = items[len(items) - index]
        
        if value[index][capacity] > value[index - 1][capacity]:
            capacity -= item["weight"]
            item["is_selected"] = True
        else:
            item["is_selected"] = False
        index -= 1

