from .scheduler import *
from . import simple_scheduler
from . import plot
from . import experiment
from .data import *
from . import lower_bound
import json
import sys
import argparse
import numpy as np
import datetime
import collections

# Multilevel argument parsing based on
# https://chase-seibert.github.io/blog/2014/03/21/python-multilevel-argparse.html
class Main(object):

    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Run Deiana Scheduler',
            usage='''<command> [<args>]
The possible commands are:
    runtime            Generate a graph of algorithm runtime
    runtime_simple     Generate a graph of the runtime, solving w/ the simplified model.
    plot_runtime       Generate a plot of the algorithm runtime
    plot_schedule      Generate a plot of the schedule
    single             Run a single workload
    simplified_single  Run a single workload w/ the simplified model
    lower_bound        Calculate lower bound using the Lagrangean Dual
    generate           Generate a workload and store in a file
    generate_single    Generate a single workload and store in a file
    solve_at_many_reconfig_speeds
    plot_makespan_vs_reconfig_time
    generate_many_workloads_many_reconfig_speeds
    solve_bounds_at_many_reconfig_speeds
            '''
        )
        parser.add_argument('command', help='Subcommand to run')
        # Exclude the remaining args since they are for the subcommands
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print("Unrecognized command")
            parser.print_help()
            exit(1)
        getattr(self, args.command)()

    # TODO: IMPORTANT - also add the cut constraints to the model.
    # TODO: Time varies a lot => should average many runs
    def runtime(self):
        parser = argparse.ArgumentParser(description='Generate a graph of algorithm runtime')
        parser.add_argument('--input-file', dest="input_file", required=True)        
        parser.add_argument('--output-file', dest="output_file", required=True)
        parser.add_argument('--solve-method', dest="solve_method", required=True, choices=['full-MILP', 'iterative', 'simplified'])
        args = parser.parse_args(sys.argv[2:])

        data = None
        with open(args.input_file, 'r') as input_file:
            schema = WorkloadSchema()
            data = schema.loads(input_file.read(), many=True)
        
        deiana_scheduler = DeianaScheduler()
        schema = ResultSchema()            
        results = []
        i = 0
        for workload in data:
            i += 1
            result = None
            if args.solve_method == 'full-MILP':
                print(f"Starting solver at {datetime.datetime.now()}")
                result = deiana_scheduler.solve(workload)
            elif args.solve_method == 'iterative':
                result = deiana_scheduler.solve_iteratively(workload)
            elif args.solve_method == 'simplified':
                result = simple_scheduler.solve(workload)
            else:
                raise Exception("Unexpected solve method")
            results.append(result)

            if args.solve_method == 'full-MILP':
                # It looks like full-MILP doesn't necessarilly finish quickly, so save intermediate results
                with open(f"{args.output_file}.tmp{i}", 'w') as output_file:
                    output_string = json.dumps(schema.dump(results, many=True), indent=4, sort_keys=True)
                    output_file.write(output_string)
            
        with open(args.output_file, 'w') as output_file:
            output_string = json.dumps(schema.dump(results, many=True), indent=4, sort_keys=True)
            output_file.write(output_string)

    # TODO: Do an initial check on how big the optimality gap is?
    # IMPROTANT: The simplified scheduler isn't consistent w/ the full scheduler as is - since the reconfiguration time will only depend on the CLB bitstream size. This will result in a bigger optimality gap since it underestimates reconfiguration time. Fix this before reading too much into the gap.
    def runtime_simple(self):
        parser = argparse.ArgumentParser(description='Generate a graph of the algorithm runtime, solving w/ the simplified model')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        data = None
        with open(args.input_file, 'r') as input_file:
            schema = WorkloadSchema()
            data = schema.loads(input_file.read(), many=True)

        results = []
        for workload in data:
            result = simple_scheduler.solve(workload)
            results.append(result)

        with open(args.output_file, 'w') as output_file:
            schema = ResultSchema()
            output_string = json.dumps(schema.dump(results, many=True), indent=4, sort_keys=True)
            output_file.write(output_string)

    def plot_runtime(self):
        parser = argparse.ArgumentParser(description='Generate a plot of the algorithm runtime')
        parser.add_argument('--input-file', dest="input_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        data = None
        with open(args.input_file, 'r') as input_file:
            schema = ResultSchema()
            data = schema.loads(input_file.read(), many=True)
        plot.plot_runtime(data)
        
        
    def single(self):
        parser = argparse.ArgumentParser(description='Run a single workload')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        parser.add_argument('--solve-iteratively', action="store_true")
        args = parser.parse_args(sys.argv[2:])

        workload = None
        with open(args.input_file, 'r') as input_file:            
            schema = WorkloadSchema()
            workload = schema.loads(input_file.read())
            
        result = None
        if args.solve_iteratively:
            print("Running iterative scheduling algorithm")
            result = DeianaScheduler().solve_iteratively(workload)
        else:
            print("Running exact scheduling algorithm")
            result = DeianaScheduler().solve(workload)
            
        if args.output_file:
            with open(args.output_file, 'w') as output_file:
                schema = ResultSchema()
                output_string = json.dumps(schema.dump(result), indent=4, sort_keys=True)
                output_file.write(output_string)
        else:        
            plot.plot_schedule(result)

    def plot_schedule(self):
        parser = argparse.ArgumentParser(description='Plot a schedule')
        parser.add_argument('--input-file', dest="input_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        result = None
        with open(args.input_file, 'r') as input_file:
            schema = ResultSchema()
            result = schema.loads(input_file.read())
        plot.plot_schedule(result)
            
    # TODO: Actually validate that the schedule makes sense
    def simplified_single(self):
        parser = argparse.ArgumentParser(description='Run a single workload w/ simplified model')
        parser.add_argument('--input-file', dest="input_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        workload = None
        with open(args.input_file, 'r') as input_file:
            schema = WorkloadSchema()
            workload = schema.loads(input_file.read())
        result = simple_scheduler.solve(workload)
        print(f"The schedule makespan is {result.makespan}")
        
    def lower_bound(self):
        # TODO: Try switching knapsack problem with a Gurobi based implementation?
        parser = argparse.ArgumentParser(description='Calculate a lower bound using the Lagrangean Dual')
        parser.add_argument('--input-file', dest="input_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        workload = None
        with open(args.input_file, 'r') as input_file:
            schema = WorkloadSchema()
            workload = schema.loads(input_file.read())
        result = lower_bound.compute_lower_bound(workload)
        print(f"Obtained the lower bound {result}")
        
    def generate(self):
        """
        Used to generate a workload where there are a number of tasks from 1 to max_num_tasks.
        This is useful for the graph of Gurobi solve time vs #tasks.
        """
        parser = argparse.ArgumentParser(description='Generate a graph of algorithm runtime')
        parser.add_argument('--reconfig-time', dest="reconfig_time", type=int, nargs='?')
        parser.add_argument('--max-num-tasks', dest="max_num_tasks", type=int, nargs='?', required=True)
        parser.add_argument('--num-samples', dest="num_samples", type=int, required=True)
        parser.add_argument('--target-file', dest="target_file")
        args = parser.parse_args(sys.argv[2:])

        workloads = []
        for num_tasks in range(1, args.max_num_tasks + 1):
            for sample in range(args.num_samples):
                workload = experiment.hard_random_workload(args.reconfig_time, num_tasks)
                workloads.append(workload)

        schema = WorkloadSchema()
        value = json.dumps(schema.dump(workloads, many=True), indent=4, sort_keys=True)
        
        if args.target_file:
            with open(args.target_file, 'w') as f:
                f.write(value)
        else:
            print(value)
        
    def generate_single(self):
        parser = argparse.ArgumentParser(description='Generate a graph of algorithm runtime')
        parser.add_argument('--reconfig-time', dest="reconfig_time", type=int, nargs='?')
        parser.add_argument('--num-tasks', dest="num_tasks", type=int, nargs='?', required=True)
        parser.add_argument('--target-file', dest="target_file")
        args = parser.parse_args(sys.argv[2:])

        workload = experiment.hard_random_workload(args.reconfig_time, args.num_tasks)

        schema = WorkloadSchema()
        value = json.dumps(schema.dump(workload), indent=4, sort_keys=True)
        
        if args.target_file:
            with open(args.target_file, 'w') as f:
                f.write(value)
        else:
            print(value)

    def generate_many_workloads_many_reconfig_speeds(self):
        """
        As the name implies, this generates a ManyWorkloadsManyReconfigSpeeds object.
        This is particularly useful for the figures where I show how makespan depends on the reconfiguration speed.
        For solving, see solve_bounds_at_many_reconfig_speeds and plotting see plot_makespan_vs_reconfig_time_many_series.
        """
        parser = argparse.ArgumentParser(description='Generate many workloads which will be solved at many reconfiguration speeds')
        parser.add_argument('--output-file', dest="output_file", required=True)
        parser.add_argument('--min-reconfig-time', dest="min_reconfig_time", required=True, type=float)
        parser.add_argument('--max-reconfig-time', dest="max_reconfig_time", required=True, type=float)
        parser.add_argument('--num-samples', dest="num_samples", required=True, type=int)
        parser.add_argument('--num-tasks', dest="num_tasks", required=True, type=int)
        parser.add_argument('--num-workloads', dest="num_workloads", required=True, type=int)
        args = parser.parse_args(sys.argv[2:])

        workloads = []
        for i in range(args.num_workloads):
            workload = experiment.hard_random_workload(None, args.num_tasks)
            workloads.append(workload)
            
        many_workloads_many_reconfig_speeds = ManyWorkloadsManyReconfigSpeeds(workloads, args.min_reconfig_time, args.max_reconfig_time, args.num_samples)
        with open(args.output_file, 'w') as output_file:
            schema = ManyWorkloadsManyReconfigSpeedsSchema()
            value = json.dumps(schema.dump(many_workloads_many_reconfig_speeds), indent=4, sort_keys=True)
            output_file.write(value)

    def many_workloads_many_reconfig_speeds_transform_workload(self):
        """
        Transforms a ManyWorkloadsManyReconfigSpeeds object so that we can analyze the impact on makespan vs reconfig time.
        """
        parser = argparse.ArgumentParser(description='Transform workloads in a ManyWorkloadsManyReconfigSpeeds')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--transformation', dest="transformation", required=True, choices=['improve-hw-impls', 'shift-hw-impls', 'split'])
        parser.add_argument('--output-file', dest="output_file", required=True)
        args = parser.parse_args(sys.argv[2:])

        schema = ManyWorkloadsManyReconfigSpeedsSchema()
        
        many_workloads_many_reconfig_speeds = None
        with open(args.input_file, 'r') as input_file:            
            many_workloads_many_reconfig_speeds = schema.loads(input_file.read())            

        transformation = None
        if args.transformation == "improve-hw-impls":
            transformation = experiment.improve_hardware_implementations
        elif args.transformation == "shift-hw-impls":
            transformation = experiment.shift_hardware_implementations
        elif args.transformation == "split":
            transformation = experiment.split_tasks
        else:
            raise Exception(f"Unrecognized transformation option {args.transformation}")
        new_workloads = list(map(transformation, many_workloads_many_reconfig_speeds.workloads))

        new_many_workloads_many_reconfig_speeds = ManyWorkloadsManyReconfigSpeeds(
            new_workloads,
            many_workloads_many_reconfig_speeds.min_reconfig_time,
            many_workloads_many_reconfig_speeds.max_reconfig_time,
            many_workloads_many_reconfig_speeds.num_samples
        )

        with open(args.output_file, 'w') as output_file:
            value = json.dumps(schema.dump(new_many_workloads_many_reconfig_speeds), indent=4, sort_keys=True)
            output_file.write(value)
            
    def solve_at_many_reconfig_speeds(self):
        parser = argparse.ArgumentParser(description='Determine optimal schedule for workload at many reconfig speeds')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])

        many_reconfig_speeds_workload = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyReconfigSpeedsWorkloadSchema()
            many_reconfig_speeds_workload = schema.loads(input_file.read())        

        workload = many_reconfig_speeds_workload.workload
        scheduler = DeianaScheduler()
        results = []
        for reconfig_time in np.linspace(
                many_reconfig_speeds_workload.min_reconfig_time,
                many_reconfig_speeds_workload.max_reconfig_time,
                many_reconfig_speeds_workload.num_samples):
            workload.reconfiguration_speed = reconfig_time
            result = scheduler.solve(workload)
            many_reconfig_speeds_result = ManyReconfigSpeedsSingleResult(
                result.schedule,
                result.makespan,
                reconfig_time
            )
            results.append(many_reconfig_speeds_result)

        workload.reconfiguration_speed = None

        overall_result = ManyReconfigSpeedsOverallResult(many_reconfig_speeds_workload, results)

        if args.output_file:
            with open(args.output_file, 'w') as output_file:
                schema = ManyReconfigSpeedsOverallResultSchema()
                output_string = json.dumps(schema.dump(overall_result), indent=4, sort_keys=True)
                output_file.write(output_string)
        else:
            plot.plot_makespan_vs_reconfig_time(overall_result)

    def solve_bounds_at_many_reconfig_speeds(self):
        parser = argparse.ArgumentParser(description='Run Deiana iterative scheduler and the simplified scheduler at many reconfig speeds')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--solve-method', dest="solve_method", required=True, choices=['iterative', 'simplified'])
        parser.add_argument('--data-series-name', dest="name", required=True, help="Added during plotting")
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])
        
        many_workloads_many_reconfig_speeds = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyWorkloadsManyReconfigSpeedsSchema()
            many_workloads_many_reconfig_speeds = schema.loads(input_file.read())

        results_by_reconfig_speed = []
        scheduler = DeianaScheduler()
        for reconfig_time in np.linspace(
                many_workloads_many_reconfig_speeds.min_reconfig_time,
                many_workloads_many_reconfig_speeds.max_reconfig_time,
                many_workloads_many_reconfig_speeds.num_samples):
            results = []
            for workload in many_workloads_many_reconfig_speeds.workloads:
                workload.reconfiguration_speed = reconfig_time
                result = None
                if args.solve_method == "iterative":
                    result = scheduler.solve_iteratively(workload)
                    results.append(result)
                elif args.solve_method == "simplified":
                    # TODO: Could be more performant to make simple_scheduler a class like the other MILP solvers
                    result = simple_scheduler.solve(workload)
                    results.append(result)
                else:
                    raise Exception("Unexpected solve method")
            results_by_reconfig_speed.append(ManyWorkloadsSingleReconfigSpeedResult(reconfig_time, results))

        many_workloads_many_reconfig_speeds_result = ManyWorkloadsManyReconfigSpeedsResult(args.name, results_by_reconfig_speed)
        if args.output_file:
            with open(args.output_file, 'w') as output_file:
                schema = ManyWorkloadsManyReconfigSpeedsResultSchema()
                output_string = json.dumps(schema.dump(many_workloads_many_reconfig_speeds_result), indent=4, sort_keys=True)
                output_file.write(output_string)
        else:
            plot.plot_makespan_vs_reconfig_time_many_series([many_workloads_many_reconfig_speeds_result])

    def plot_makespan_vs_reconfig_time(self):
        parser = argparse.ArgumentParser(description='Plot makespan vs reconfig time')
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])

        result = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyReconfigSpeedsOverallResultSchema()
            result = schema.loads(input_file.read())

        plot.plot_makespan_vs_reconfig_time(result, args.output_file)

    def plot_makespan_vs_reconfig_time_many_series(self):
        """
        This creates a makespan vs reconfiguration speed figure with multiple series of data (ex: upper bound and lower bound)
        See also generate_many_workloads_many_reconfig_speeds and solve_bounds_at_many_reconfig_speeds.
        """
        parser = argparse.ArgumentParser(description='Plot average makespan vs reconfig time')
        # Thank you to https://stackoverflow.com/a/36170308/16556113
        parser.add_argument('--input-file', dest="input_files", action="append")
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])

        all_series_results = []
        schema = ManyWorkloadsManyReconfigSpeedsResultSchema()
        for filename in args.input_files:
            with open(filename, 'r') as input_file:
                result = schema.loads(input_file.read())
                all_series_results.append(result)
        plot.plot_makespan_vs_reconfig_time_many_series(all_series_results, args.output_file)                

    def plot_solve_time_vs_num_tasks_many_series(self):
        """
        This creates a plot of the scheduler solve time vs the number of tasks.
        See also the runtime method, which actually runes the schedulers.
        """
        parser = argparse.ArgumentParser(description='Plot the scheduler solve time vs num tasks')
        parser.add_argument('--input-file', dest="input_files", nargs=2, metavar=('series-name', 'filename'), action="append")
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])

        all_series_results = {}
        schema = ResultSchema()
        print(args.input_files)
        for input_args in args.input_files:
            series_name = input_args[0]
            input_filename = input_args[1]
            with open(input_filename, 'r') as input_file:
                results = schema.loads(input_file.read(), many=True)
                all_series_results[series_name] = results
        plot.plot_solve_time_vs_num_tasks_many_series(all_series_results, args.output_file)

    def plot_avg_num_sw_tasks_vs_reconfig_time(self):
        parser = argparse.ArgumentParser(description="Plot the avg number of SW tasks vs reconfig time")
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])
        
        overall_result = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyWorkloadsManyReconfigSpeedsResultSchema()            
            overall_result = schema.loads(input_file.read())

        plot.plot_avg_num_sw_tasks_vs_reconfig_time(overall_result, args.output_file)

    def plot_avg_num_partitions_vs_reconfig_time(self):
        parser = argparse.ArgumentParser(description="Plot the avg number of FPGA partitions vs reconfig time")
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])
        
        overall_result = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyWorkloadsManyReconfigSpeedsResultSchema()            
            overall_result = schema.loads(input_file.read())
        plot.plot_avg_num_partitions_vs_reconfig_time(overall_result, args.output_file)

    def plot_avg_implementation_size_vs_reconfig_time(self):
        parser = argparse.ArgumentParser(description="Plot the avg FPGA implementation size vs reconfig time")
        parser.add_argument('--input-file', dest="input_file", required=True)
        parser.add_argument('--output-file', dest="output_file")
        args = parser.parse_args(sys.argv[2:])
        
        overall_result = None
        with open(args.input_file, 'r') as input_file:
            schema = ManyWorkloadsManyReconfigSpeedsResultSchema()            
            overall_result = schema.loads(input_file.read())
        plot.plot_avg_implementation_size_vs_reconfig_time(overall_result, args.output_file)

    def calculate_mape(self):
        parser = argparse.ArgumentParser(description="Calculate the MAPE")
        parser.add_argument('--actual-result', dest="actual", required=True)
        parser.add_argument('--estimated-result', dest="estimated", required=True)
        args = parser.parse_args(sys.argv[2:])
        
        schema = ResultSchema()
        actual_results = None
        estimated_results = None
        with open(args.actual, 'r') as input_file:
            actual_results = schema.loads(input_file.read(), many=True)
        with open(args.estimated, 'r') as input_file:
            estimated_results = schema.loads(input_file.read(), many=True)

        grouped_by_task_num = collections.defaultdict(list)
        # Assumes that the workloads were processed in the same order
        for actual_result, estimated_result in zip(actual_results, estimated_results):
            error = abs( (actual_result.makespan - estimated_result.makespan) / actual_result.makespan )
            grouped_by_task_num[len(actual_result.schedule)].append(error)

        print(f"num_tasks: MAPE")
        for num_tasks, errors in grouped_by_task_num.items():
            mape = (100 / float(len(errors))) * sum(errors)
            print(f"{num_tasks}: {mape}")
        
if __name__ == "__main__":
    Main()
