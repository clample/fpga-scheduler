import random
from .data import *
import operator

def hard_random_workload(reconfiguration_speed, num_hw_tasks):
    
    fpga_resources = {'CLB': 10000, 'DSP': 10000, 'BRAM': 10000}
    num_software_components = 1

    tasks = []
    for task_id in range(num_hw_tasks):

        sw_impl_runtime = random.randint(5000, 10000)
        sw_implementations = [SoftwareImplementation(f"sw_impl_t{task_id}_0", sw_impl_runtime)]
        hw_implementations = []
        num_hw_implementations = random.randint(2, 10) # Randomly select the number of implementations

        # Every task can be implemented with less than 3000 resources
        impl_size = random.randint(1, 3000) 
        impl_runtime = random.randint(500, sw_impl_runtime)
        implementation = HardwareImplementation(f"hw_impl_t{task_id}_0",
                                                impl_size, impl_size, impl_size, impl_runtime)
        hw_implementations.append(implementation)
        

        for impl_number in range(1, num_hw_implementations):
            # The new implementation should be larger than the last one
            impl_size = random.randint(impl_size, fpga_resources['CLB'])
            # The new implementation should be faster than the last one (since it's larger)
            impl_runtime = random.randint(500, impl_runtime)
            
            implementation = HardwareImplementation(f"hw_impl_t{task_id}_{impl_number}",
                                                    impl_size, impl_size, impl_size, impl_runtime)
            hw_implementations.append(implementation)
        
        task = Task(f"t_{task_id}", hw_implementations, sw_implementations)
        tasks.append(task)

    return Workload.create_workload(tasks, reconfiguration_speed, num_software_components, fpga_resources)

def bad_random_workload(reconfiguration_speed, num_hw_tasks):
    """
    Generate random workload where increasing the Area of a HW implementation by factor x only 
    improves the execution time by a factor slightly less than x.
    """
    pass # TODO

def improve_hardware_implementations(workload):
    return transform_hardware_implementations(workload, improve_hardware_implementations_hw_transform)

def improve_hardware_implementations_hw_transform(hw_implementations):
    """
    Improve the execution time of all hardware implementations.
    This allows us to compare how the possible Area vs. Perf tradeoff affects the impact of reconfiguration speed.
    """
    new_hardware_implementations = []
    for hw_impl_index, hw_impl in enumerate(hw_implementations):
        new_execution_time = int(0.3 * hw_impl.execution_time)
        new_hw_impl = HardwareImplementation(
            hw_impl.name,
            hw_impl.clb_requirement,
            hw_impl.dsp_requirement,
            hw_impl.bram_requirement,
            new_execution_time)
        new_hardware_implementations.append(new_hw_impl)
    return new_hardware_implementations

def shift_hardware_implementations(workload):
    return transform_hardware_implementations(workload, shift_hardware_implementations_hw_transform)

def shift_hardware_implementations_hw_transform(hw_implementations):
    """
    Worsens the execution time of all hardware implementations except for the fastest/largest.
    This is certainly very similar to improve_hardware_implementations, 
    but the difference is that the fastest/largest implementation remains unchanged.
    """
    new_hardware_implementations = []
    for hw_impl_index, hw_impl in enumerate(hw_implementations):
        new_execution_time = None
        if hw_impl_index == 0:
            new_execution_time = hw_impl.execution_time
        else:
            new_execution_time = int(hw_impl.execution_time * 4)
        new_hw_impl = HardwareImplementation(
            hw_impl.name,
            hw_impl.clb_requirement,
            hw_impl.dsp_requirement,
            hw_impl.bram_requirement,
            new_execution_time)
        new_hardware_implementations.append(new_hw_impl)
    return new_hardware_implementations

def drop_fastest_hardware_implementations(workload):
    """
    For each task, removes the fastest hardware implementation.
    """
    return transform_hardware_implementations(workload, drop_fastest_hardware_implementations_hw_transform)

def drop_fastest_hardware_implementations_hw_transform(hw_implementations):
    if len(hw_implementations) == 1:
        return hw_implementations
    return hw_implementations[1:]
    

def split_tasks(workload):
    """
    Split the tasks and allow them to have one faster/larger hw implementation.
    """
    new_task_list = []
    for task in workload.task_list:
        new_t1_hw_impls = []
        new_t2_hw_impls = []
        new_t1_sw_impls = []
        new_t2_sw_impls = []

        for sw_impl in task.software_implementations:
            # Split the SW implementation into two implementations which each take half as long.
            # Since wthe execution time is currently an int, I cast to an int and add 1 just to be conservative.
            new_execution_time = int(sw_impl.execution_time * 0.5) + 1
            new_t1_sw_impl = SoftwareImplementation(f"{sw_impl.name} - a", new_execution_time)
            new_t2_sw_impl = SoftwareImplementation(f"{sw_impl.name} - b", new_execution_time)
            new_t1_sw_impls.append(new_t1_sw_impl)
            new_t2_sw_impls.append(new_t2_sw_impl)
        for hw_impl in task.hardware_implementations:
            # When splitting the HW Implementation into two implementations, they each take the same amount of time but require fewer resources.
            new_execution_time = hw_impl.execution_time
            new_resources_clb = int((hw_impl.clb_requirement / 2) * 1.2)
            new_resources_dsp = int((hw_impl.dsp_requirement / 2) * 1.2)
            new_resources_bram = int((hw_impl.bram_requirement / 2) * 1.2)

            new_t1_hw_impl = HardwareImplementation(f"{hw_impl.name} - a", new_resources_clb, new_resources_dsp, new_resources_bram, new_execution_time)
            new_t2_hw_impl = HardwareImplementation(f"{hw_impl.name} - b", new_resources_clb, new_resources_dsp, new_resources_bram, new_execution_time)
            new_t1_hw_impls.append(new_t1_hw_impl)
            new_t2_hw_impls.append(new_t2_hw_impl)

        
        # In addition to splitting the HW and SW implementations, we add a larger, faster HW implementation
        # Rather than do it in a random fashion, the current largest fastest implementation is scaled.
        fastest_t1_hw_impl = sorted(new_t1_hw_impls, key=operator.attrgetter('execution_time'))[0]
        fastest_t2_hw_impl = sorted(new_t2_hw_impls, key=operator.attrgetter('execution_time'))[0]
        new_t1_hw_impl = HardwareImplementation(
            f"{task.name} - extra hw impl - a",
            int(1.6 * fastest_t1_hw_impl.clb_requirement),
            int(1.6 * fastest_t1_hw_impl.dsp_requirement),
            int(1.6 * fastest_t1_hw_impl.bram_requirement),
            int(fastest_t1_hw_impl.execution_time / 3.0)
        )
        new_t2_hw_impl = HardwareImplementation(
            f"{task.name} - extra hw impl - b",
            int(1.6 * fastest_t2_hw_impl.clb_requirement),
            int(1.6 * fastest_t2_hw_impl.dsp_requirement),
            int(1.6 * fastest_t2_hw_impl.bram_requirement),
            int(fastest_t2_hw_impl.execution_time / 3.0)
        )
        new_t1_hw_impls.append(new_t1_hw_impl)
        new_t2_hw_impls.append(new_t2_hw_impl)
        
        new_t1 = Task(f"{task.name} - a", new_t1_hw_impls, new_t1_sw_impls)
        new_t2 = Task(f"{task.name} - b", new_t2_hw_impls, new_t2_sw_impls)
        new_task_list.append(new_t1)
        new_task_list.append(new_t2)

    return Workload.create_workload(
        new_task_list,
        workload.reconfiguration_speed,
        len(workload.software_components),
        workload.fpga_resources)


def transform_hardware_implementations(workload, hw_impl_transform):
    """
    Apply the hw_impl_transform function to all hardware implementations of the workload.
    """
    new_task_list = []
    for task in workload.task_list:
        original_hardware_implementations = sorted(task.hardware_implementations, key=operator.attrgetter('execution_time'))
        new_hardware_implementations = hw_impl_transform(original_hardware_implementations)
        new_task_list.append(Task(task.name, new_hardware_implementations, task.software_implementations))
        
    # The reason for using `create_workload` rather than `__init__` is that the number of required HW components may have changed.
    # It's necessary to recompute that using region_solver.
    return Workload.create_workload(
        new_task_list,
        workload.reconfiguration_speed,
        len(workload.software_components),
        workload.fpga_resources)
