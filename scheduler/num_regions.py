from pyomo.environ import *

# See section 5.4 from Deiana thesis
# TODO: To be fair - I should use this to set the number of regions for the Deiana scheduler and for the simplified scheduler. I think that this will require some refactoring, though, that I can do later after I'm more sure that it works
class RegionSolver:

    def __init__(self):
        self.abstract_model = RegionSolver.create_abstract_model()
        self.solver_factory = SolverFactory('gurobi')
        
    def create_abstract_model():
        model = AbstractModel()
        model.tasks = Set()
        model.resources = Set()
        model.fpga_resources = Param(model.resources, within=NonNegativeIntegers)
        
        model.hardware_implementations = Set()

        model.resource_requirements = Param(model.hardware_implementations * model.resources, within=NonNegativeIntegers)

        model.possible_assignments = Set(within=model.tasks * model.hardware_implementations)

        model.task_assigned_region = Var(model.possible_assignments, within=Boolean)

        model.objective = Objective(rule=RegionSolver.optimize_regions, sense=maximize)

        model.one_impl_constraint = Constraint(model.tasks, rule=RegionSolver.one_impl_constraint)
        model.resource_usage_constraint = Constraint(model.resources, rule=RegionSolver.resource_usage_constraint)
        return model

    def solve(self, tasks, fpga_resources):
        data = self.model_data(tasks, fpga_resources)
        instance = self.abstract_model.create_instance(data)
        self.solver_factory.solve(instance)
        return self.process_result(instance)

    def process_result(self, solved_instance):
        return round(value(solved_instance.objective))

    def model_data(self, tasks, fpga_resources):
        hardware_implementations = [
            hw_impl for task in tasks for hw_impl in task.hardware_implementations
        ]

        resource_requirements = {}
        for impl in hardware_implementations:
            resource_requirements[(impl.name, 'CLB')] = impl.clb_requirement
            resource_requirements[(impl.name, 'DSP')] = impl.dsp_requirement
            resource_requirements[(impl.name, 'BRAM')] = impl.bram_requirement

        possible_assignments = [
            (task.name, impl.name)
            for task in tasks
            for impl in task.hardware_implementations
        ]

        return { None: {
            'tasks': { None: [ task.name for task in tasks ] },
            'resources': { None: fpga_resources.keys() },
            'fpga_resources': fpga_resources,
            'hardware_implementations': { None: [ impl.name for impl in hardware_implementations  ] },
            'resource_requirements': resource_requirements,
            'possible_assignments': { None: possible_assignments }
        }}

    def optimize_regions(model):
        return sum(model.task_assigned_region[possible_assignment] for possible_assignment in model.possible_assignments)

    def one_impl_constraint(model, task):
        implementations_for_task = [
            (t, i) for (t, i) in model.possible_assignments
            if t == task
        ]
        return (None, sum(model.task_assigned_region[mapping] for mapping in implementations_for_task), 1)

    def resource_usage_constraint(model, resource):
        return (None, sum(model.task_assigned_region[(t,i)] * model.resource_requirements[(i, resource)] for (t,i) in model.possible_assignments), model.fpga_resources[resource])
        
        
