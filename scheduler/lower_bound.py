import math
import numpy as np
from . import knapsack_milp
from . import data
from nsopy.methods.subgradient import SubgradientMethod
from nsopy.methods.bundle import BundleMethod
from nsopy.loggers import GenericMethodLogger, EnhancedDualMethodLogger

def compute_lower_bound(workload):
    lagrangian = SimplifiedSchedulingLagrangian(workload)
    
    #method = SubgradientMethod(lagrangian.oracle, lagrangian.projection_function, sense='max', dimension=lagrangian.dimension)

    method = BundleMethod(lagrangian.oracle, lagrangian.projection_function, dimension=lagrangian.dimension, sense='max')
    logger = EnhancedDualMethodLogger(method)
    method.set_dual_domain(type='positive orthant')

    print(f"Starting lower bound {lagrangian.lower_bound}")
    for iteration in range(200):        
        method.dual_step()
        print(f"Doing a step {logger.d_k_iterates[-1]} {logger.lambda_k_iterates[-1]}")
    

class SimplifiedSchedulingLagrangian:

    def __init__(self, workload):
        self.knapsack_solver = knapsack_milp.KnapsackSolver()
        self.workload = workload
        self.upper_bound = self.workload.get_max_execution_time()
        self.lower_bound = self.workload.simplified_scheduling_lower_bound()

        self.num_hw_components = len(self.workload.hardware_components)
        self.num_sw_components = len(self.workload.software_components)        
        
        self.task_indices = { task_name: index for index, task_name in enumerate(self.workload.tasks.keys()) }
        self.impl_indices = { impl.name: index for index, impl in enumerate(self.workload.hardware_implementations) }
        self.dimension = len(self.workload.tasks) + len(self.workload.hardware_implementations) * len(workload.resources)

    def oracle(self, multipliers):
        """
        Computes L(lambda, pi) for the given Lagrangian multipliers.
        Returns the value as well as a subgradient.       
        """
        lambda_multipliers = multipliers[0:len(self.tasks)]
        # NOTE: I think I need to scale the pi multipliers before and after passsing to nsopy
        # Makes sense to test this more, though, and write up what/why I'm doing this in the thesis
        pi_multipliers = (1 / math.sqrt(self.num_hw_components)) * multipliers[len(self.workload.tasks):].reshape((-1, len(self.workload.resources)))  

        satisfies_property = lambda z: self.compute_solution_for_time_capacity(lambda_multipliers, pi_multipliers, z)[0] <= z
        new_lower_bound = self.binary_search(satisfies_property)
        
        value, diff = self.compute_solution_for_time_capacity(lambda_multipliers, pi_multipliers, new_lower_bound)
        # In the nsopy convention, the first return value should be the selected schedule
        # This isn't actually used by nsopy, though, so for now we don't return anything
        # TODO: I think that there's a bug here. Aren't I supposed to be returning the `new_lower_bound`? Also, I can update self.lower_bound as the bounds improve? On the other hand - maybe that would throw off the subgradient optimization.
        return 0, value, diff

    def projection_function(self, multipliers):
        # Need to project all of the multipliers to be positive (I think)
        # TODO: I don't think that this is used how I think it's used? Looks like it's only called during setup, so can't be too important
        return np.maximum(multipliers, 0)


    def compute_solution_for_time_capacity(self,
                                           lambda_multipliers,
                                           pi_multipliers,
                                           time_capacity):
        """
        This corresponds to L(lambda, pi, z)
        """
        sw_subproblem_value, sw_num_times_scheduled = self.solve_sw_subproblem(lambda_multipliers, time_capacity)
        hw_subproblem_value, hw_num_times_scheduled, hw_resource_usage = self.solve_hw_subproblem(lambda_multipliers, pi_multipliers, time_capacity)        
        resource_subproblem_values = self.solve_resource_subproblems(pi_multipliers)

        value = (time_capacity +
                 np.sum(lambda_multipliers) -
                 (sw_subproblem_value * self.num_sw_components) -
                 (hw_subproblem_value * self.num_hw_components) -
                 np.sum(resource_subproblem_values))

        
        lambda_diff = (1 -
                (self.num_sw_components * sw_num_times_scheduled) -
                (self.num_hw_components * hw_num_times_scheduled))

        allocated_resources = self.workload.fpga_resources_np / float(self.num_hw_components)
        # NOTE: There's some scaling to convert between the two pi variable spaces
        # Should probably explain this more in the thesis
        pi_diff = math.sqrt(self.num_hw_components) * (hw_resource_usage - allocated_resources)

        diff = np.concatenate((lambda_diff, pi_diff.ravel()))        
        # TODO: Hopefully the diff has the right sign? Otherwise, try reversing things lol
        return value, diff
    
    def solve_sw_subproblem(self, lambda_multipliers, time_capacity):
        items = []
        for task in self.workload.tasks.values():
            task_indice = self.task_indices[task.name]
            for impl in task.software_implementations:
                value = lambda_multipliers[task_indice]
                weight = impl.execution_time
                item = {
                    "weight": weight,
                    "value": value,
                    "task": task.name,
                    "implementation": impl.name
                }
                if value > 0:
                    items.append(item)

        num_times_task_scheduled = np.zeros(len(self.workload.tasks), dtype=int)        
        if len(items) == 0:
            return 0, num_times_task_scheduled

        result = self.knapsack_solver.solve(items, time_capacity)
        value = result["value"]
        for item in result["items"]:
            if item["is_selected"]:
                task = item["task"]
                task_indice = self.task_indices[task]
                num_times_task_scheduled[task_indice] += 1
        
        return value, num_times_task_scheduled

    def solve_hw_subproblem(self, lambda_multipliers, pi_multipliers, time_capacity):
        items = []
        for task in self.workload.tasks.values():
            task_index = self.task_indices[task.name]
            for impl in task.hardware_implementations:
                impl_indice = self.impl_indices[impl.name]
                if isinstance(impl, data.HardwareImplementation):
                    resource_requirements = impl.resource_requirements()
                    value = lambda_multipliers[task_index] - np.sum(pi_multipliers[impl_indice] * resource_requirements)
                    weight = impl.execution_time + np.sum(self.workload.reconfiguration_speed * resource_requirements)
                    item = {
                        "weight": weight,
                        "value": value,
                        "task": task,
                        "implementation": impl.name
                    }
                    if value > 0:
                        items.append(item)

        num_times_task_scheduled = np.zeros(len(self.workload.tasks), dtype=int)
        resource_usage_per_impl = np.zeros((len(self.workload.hardware_implementations), len(self.workload.resources)) , dtype=int)
        if len(items) == 0:
            return 0, num_times_task_scheduled, resource_usage_per_impl
        
        result = self.knapsack_solver.solve(items, time_capacity)
        value = result["value"]
        for item in result["items"]:
            if item["is_selected"]:
                task = item["task"]
                impl_name = item["implementation"]
                impl_indice = self.impl_indices[impl_name]
                task_indice = self.task_indices[task]
                num_times_task_scheduled[task_indice] += 1
                impl = self.workload.implementations[impl_name]
                resource_usage_per_impl[impl_indice] += impl.resource_requirements()
        return value, num_times_task_scheduled, resource_usage_per_impl


    def solve_resource_subproblems(self, pi_multipliers):
        return pi_multipliers @ self.workload.fpga_resources_np
        
    def binary_search(self, is_good):
        search_lower_bound = self.lower_bound - 1
        search_upper_bound = self.upper_bound

        while 1 < search_upper_bound - search_lower_bound:
            middle = (search_lower_bound + search_upper_bound) // 2
            if is_good(middle):
                search_upper_bound = middle
            else:
                search_lower_bound = middle
        return search_upper_bound
