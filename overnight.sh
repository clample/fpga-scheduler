set -o pipefail
set -e

echo "Starting solve_bounds_at_many_reconfig_speeds unsplit iterative"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-unsplit-tasks-2.json --solve-method iterative --data-series-name iterative --output-file data/many-workloads-many-reconfig-speeds-unsplit-tasks-2-iterative-solved.json

echo "Starting solve_bounds_at_many_reconfig_speeds unsplit simplified"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-unsplit-tasks-2.json --solve-method simplified --data-series-name simplified --output-file data/many-workloads-many-reconfig-speeds-unsplit-tasks-2-simplified-solved.json

echo "Starting solve_bounds_at_many_reconfig_speeds split iterative"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-split-tasks-2.json --solve-method iterative --data-series-name iterative --output-file data/many-workloads-many-reconfig-speeds-split-tasks-2-iterative-solved.json

echo "Starting solve_bounds_at_many_reconfig_speeds split simplified"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-split-tasks-2.json --solve-method simplified --data-series-name simplified --output-file data/many-workloads-many-reconfig-speeds-split-tasks-2-simplified-solved.json

echo "Starting solve_bounds_at_many_reconfig_speeds iterative"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-2.json --solve-method iterative --data-series-name iterative --output-file data/many-workloads-many-reconfig-speeds-2-iterative-solved.json

echo "Starting solve_bounds_at_many_reconfig_speeds simplified"
python -m scheduler solve_bounds_at_many_reconfig_speeds --input-file data/many-workloads-many-reconfig-speeds-2.json --solve-method simplified --data-series-name simplified --output-file data/many-workloads-many-reconfig-speeds-2-simplified-solved.json

echo "Starting performance-benchmark-2.json with MILP"
python -m scheduler runtime --input-file data/performance-benchmark-2.json --output-file data/performance-benchmark-2-result-full-milp.json --solve-method full-MILP
