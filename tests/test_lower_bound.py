import pytest
from simplified import lower_bound

@pytest.mark.skip(reason="Skip this for now")
def test_binary_search():
    items = [1, 2, 100, 100, 100, 500, 700]
    is_good = lambda index: items[index] >= 100

    assert lower_bound.binary_search(0, 6, is_good) == 2

@pytest.mark.skip(reason="Skip this for now")
def test_lower_bound1():
    task_runtimes = [
        [ 100, 200, 400, 150 ],
        [ 200, 250, 410, 170 ],
        [ 90, 110, 340, 100 ],
        [ 140, 300, 450, 130 ],
        [ 130, 450, 500, 200 ]
    ]

    computed_bound = lower_bound.compute_lower_bound(task_runtimes)
    assert computed_bound == 340
