import pytest
from scheduler import knapsack_milp

def test_knapsack():
    # Test data from P04 https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html
    capacity = 50
    items = [
        { "weight": 31, "value": 70, "id": "task_0" },
        { "weight": 10, "value": 20, "id": "task_1" },
        { "weight": 20, "value": 39, "id": "task_2" },
        { "weight": 19, "value": 37, "id": "task_3" },
        { "weight": 4, "value": 7, "id": "task_4" },
        { "weight": 3, "value": 5, "id": "task_5" },
        { "weight": 6, "value": 10, "id": "task_6" }
    ]

    knapsack_solver = knapsack_milp.KnapsackSolver()

    expected_result = {
        "task_0": True,
        "task_1": False,
        "task_2": False,
        "task_3": True,
        "task_4": False,
        "task_5": False,
        "task_6": False
    }

    solution = knapsack_solver.solve(items, capacity)["items"]
    actual_result = {}
    for item in solution:
        actual_result[item["id"]] = item["is_selected"]
    
    assert actual_result == expected_result


@pytest.mark.skip(reason="Skip this for now")
def test_knapsack2():
    # Test data from P08 https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html
    capacity = 6404180
    items = [
        knapsack.Item(382745, 825594),
        knapsack.Item(799601, 1677009),
        knapsack.Item(909247, 1677009),
        knapsack.Item(729069, 1677009),
        knapsack.Item(467902, 1677009),
        knapsack.Item(44328, 97426),
        knapsack.Item(34610, 69666),
        knapsack.Item(698150, 1296457), 
        knapsack.Item(823460, 1679693), 
        knapsack.Item(903959, 1902996),
        knapsack.Item(853665, 1844992),
        knapsack.Item(551830, 1049289),
        knapsack.Item(610856, 1252836),
        knapsack.Item(670702, 1319836),
        knapsack.Item(488960, 953277),
        knapsack.Item(951111, 2067538),
        knapsack.Item(323046, 675367),
        knapsack.Item(446298, 853655),
        knapsack.Item(931161, 1826027),
        knapsack.Item(31385, 65731),
        knapsack.Item(496951, 901489),
        knapsack.Item(264724, 577243),
        knapsack.Item(224916, 466257),
        knapsack.Item(169684, 369261)
    ]

    expected_result = [True, True, False, True, True, True, False, False, False, True,
                       True, False, True, False, False,  True, False, False, False, False,
                       False, True, True, True ]

    
    actual_result = knapsack.solve(items, capacity)["items"]
    assert actual_result == expected_result
